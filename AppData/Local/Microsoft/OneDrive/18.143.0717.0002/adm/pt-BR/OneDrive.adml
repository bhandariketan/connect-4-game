<?xml version="1.0" encoding="utf-8"?>
<!-- (c) 2016 Microsoft Corporation -->
<policyDefinitionResources xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" revision="1.0" schemaVersion="1.0" xmlns="http://www.microsoft.com/GroupPolicy/PolicyDefinitions">
  <displayName>Configurações de Política de Grupo do OneDrive</displayName>
  <description>Várias configurações da política de grupo do cliente de sincronização do OneDrive, especialmente para definir configurações específicas de funcionalidade empresarial no cliente.</description>
  <resources>
    <stringTable>
      <!-- general -->
      <string id="OneDriveNGSCSettingCategory">OneDrive</string>
      
      <!-- block syncing personal OneDrive -->
      <string id="DisablePersonalSync">Impedir que os usuários sincronizem contas pessoais do OneDrive</string>
      <string id="DisablePersonalSync_help">Esta configuração permite impedir que os usuários sincronizem arquivos do OneDrive do consumidor (com base em conta da Microsoft). Por padrão, os usuários têm permissão para sincronizar contas pessoais do OneDrive.

Se você habilitar esta configuração, os usuários não poderão configurar uma relação de sincronização para suas contas pessoais do OneDrive. Se anteriormente eles sincronizavam uma conta pessoal do OneDrive, eles verão um erro ao iniciar o cliente de sincronização, mas seus arquivos permanecerão no disco.

Se você desabilitar esta configuração, os usuários terão permissão para sincronizar as contas pessoais do OneDrive.</string>

      <!-- turn on enterprise tier cadence for app updates -->
      <string id="EnableEnterpriseUpdate">Atrasar a atualização do OneDrive.exe até a segunda onda de lançamentos</string>
      <string id="EnableEnterpriseUpdate_help">As atualizações do OneDrive.exe são implantadas em duas ondas. A primeira onda começa quando uma atualização fica disponível e, normalmente, sua conclusão leva de uma a duas semanas. A segunda onda começa após a conclusão da primeira.

Esta configuração impede que os clientes de sincronização do OneDrive sejam atualizados até a segunda onda. Isso lhe dá tempo extra para a preparação a atualizações futuras. 

Por padrão, as atualizações são instaladas assim que estiverem disponíveis durante a primeira onda.

Se você habilitar esta configuração, os clientes de sincronização do OneDrive no seu domínio serão atualizados durante a segunda onda, várias semanas após o amplo lançamento das atualizações para os clientes do Office 365.

Se você desabilitar esta configuração, os clientes de sincronização do OneDrive serão atualizados assim que as atualizações estiverem disponíveis durante a primeira onda.</string>

      <!-- set default location of the OneDrive folder -->
      <string id="DefaultRootDir">Definir o local padrão para a pasta do OneDrive</string>
      <string id="DefaultRootDir_help">Esta configuração permite definir um caminho específico como o local padrão da pasta OneDrive quando os usuários passam pelo assistente Bem-vindo ao OneDrive ao configurar a sincronização de arquivos. Por padrão, o caminho está em %userprofile%.

Se você habilitar essa configuração, o local da pasta - {tenant name} do OneDrive local escolherá como padrão o caminho que você especificar no arquivo OneDrive.admx.

Se você desabilitar essa configuração, o local da pasta - {tenant name} do OneDrive local escolherá como padrão o %userprofile%.</string>

      <!-- disable changing the default location of the OneDrive folder -->
      <string id="DisableCustomRoot">Impedir que os usuários alterem o local da pasta do OneDrive</string>
      <string id="DisableCustomRoot_help">Esta configuração permite impedir que os usuários alterem o local da sua pasta de sincronização do OneDrive.

Se você habilitar esta configuração, os usuários não poderão alterar o local do OneDrive - a pasta {tenant name} durante o assistente Bem-vindo ao OneDrive. Isso força os usuários a usar o local padrão, ou, se você tiver definido a configuração "Definir o local padrão para a pasta do OneDrive", garante que todos os usuários tenham a pasta do OneDrive local no lugar que você especificou.

Se você desabilitar esta configuração, os usuários poderão alterar o local de sua pasta de sincronização durante o assistente Bem-vindo ao OneDrive.</string>

      <!-- Enable Office Integration for coauthoring and in-app sharing -->
      <string id="EnableAllOcsiClients">Coautoria e compartilhamento no aplicativo para arquivos do Office</string>
      <string id="EnableAllOcsiClients_help">Essa configuração permite a coautoria e o compartilhamento no aplicativo ao vivo para arquivos do Office abertos localmente no computador. A coautoria e o compartilhamento no aplicativo para arquivos do Office são permitidos por padrão. (A coautoria está disponível no Office 2013 e 2016).

Se você habilitar essa configuração, a coautoria e o compartilhamento no aplicativo do Office serão habilitados, mas os usuários poderão desativá-los na guia Office no cliente de sincronização se desejarem.

Se você desabilitar essa configuração, a coautoria e o compartilhamento no aplicativo para arquivos do Office será desabilitado e a guia Office está oculta no cliente de sincronização. Se você desabilitar essa configuração, a configuração "Os usuários podem escolher como lidar com os arquivos do Office em conflito" atuará como desabilitada e, em caso de conflitos de arquivo, o arquivo será bifurcado.</string>


      <!-- Enable hold the file for handling Office conflicts -->
      <string id="EnableHoldTheFile">Os usuários podem escolher como lidar com os arquivos do Office em conflito</string>
      <string id="EnableHoldTheFile_help">Esta configuração determina o que acontece quando há um conflito entre as versões de arquivo do Office 2016 durante a sincronização. Por padrão, os usuários têm permissão para decidir se desejam mesclar as alterações ou manter as duas cópias. Os usuários também podem configurar o cliente de sincronização para que sempre bifurque o arquivo e mantenha as duas cópias. (Esta opção só está disponível para o Office 2016. Com as versões anteriores do Office, o arquivo sempre é bifurcado e ambas as cópias são mantidas.)

Se você habilitar essa configuração, os usuários poderão decidir se desejam mesclar alterações ou manter as duas cópias. Os usuários também podem configurar o cliente de sincronização para sempre bifurcar arquivo e manter as duas cópias.

Se você desabilitar essa configuração, o arquivo sempre é bifurcado e ambas as cópias são mantidas no caso de um conflito de sincronização. A definição da configuração no cliente de sincronização é desabilitada.</string>

      <!-- Enable Automatic Upload Bandwidth Limiting -->
      <string id="AutomaticUploadBandwidthPercentage">Definir a porcentagem máxima de largura de banda de upload usada pelo OneDrive.exe</string>
      <string id="AutomaticUploadBandwidthPercentage_help">Esta configuração permite que você configure a porcentagem máxima da largura de banda disponível no computador que a sincronização do OneDrive usará para upload. (O OneDrive só usa essa largura de banda ao sincronizar arquivos.) A largura de banda disponível para um computador está em constante mudança para que uma porcentagem permita que a sincronização responda a ambos os aumentos e diminuições da disponibilidade de largura de banda durante a sincronização em segundo plano. Quanto menor a porcentagem de largura de banda que a sincronização do OneDrive pode pegar, tanto mais lentamente o computador sincronizará os arquivos. Recomendamos um valor de 50% ou superior. A sincronização permite limitar o upload periodicamente permitindo que o mecanismo de sincronização chegue à velocidade máxima de um minuto e, em seguida, diminuindo a velocidade até a porcentagem de upload definida por esta configuração. Isso permite dois cenários principais. Primeiro, um arquivo muito pequeno será carregado rapidamente porque se encaixa no intervalo onde a sincronização está medindo a velocidade máxima possível. Segundo, para qualquer upload de longa duração, a sincronização continuará otimizando a velocidade de upload com base no valor de porcentagem definido por esta configuração.

Se você habilitar esta configuração, os computadores afetados por essa política usarão a porcentagem de largura de banda máxima que você especificar.

Se você desabilitar esta configuração, os computadores permitirão que os usuários determinem quanta largura de banda de upload eles poderão usar.

Se você habilitar ou desabilitar esta configuração, não retorne a configuração para Não Configurado. Isso não alterará a configuração, e a última configuração permanecerá em vigor.</string>

      <!-- Enable Upload Bandwidth Limiting -->
      <string id="UploadBandwidthLimit">Definir a largura de banda de upload máxima usada pelo OneDrive.exe</string>
      <string id="UploadBandwidthLimit_help">Esta configuração permite configurar a largura de banda máxima disponível no computador que a sincronização do OneDrive usará para fazer upload. (O OneDrive só usa essa largura de banda ao sincronizar arquivos.) Esse limite de largura de banda é uma taxa fixa em quilobytes por segundo. Quanto menor a largura de banda que a sincronização do OneDrive pode pegar, tanto mais lentamente o computador sincronizará os arquivos. A taxa mínima que pode ser definida é 1 KB/s, e a taxa máxima é de 100000 KB/s. Qualquer entrada inferior a 50 KB/s definirá o limite como 50 KB/s, mesmo que a interface do usuário mostre a taxa inserida.

Se você habilitar esta configuração, os computadores afetados por esta política usarão a largura de banda de upload máxima que você especificar.

Se você desabilitar esta configuração, os computadores permitirão que os usuários determinem quanta largura de banda de upload eles poderão usar.</string>

      <!-- Enable Download Bandwidth Limiting -->
      <string id="DownloadBandwidthLimit">Definir a largura de banda de download máxima usada pelo OneDrive.exe</string>
      <string id="DownloadBandwidthLimit_help">Esta configuração permite definir a largura de banda máxima disponível no computador que a sincronização do OneDrive usará para fazer download. (O OneDrive só usa essa largura de banda ao sincronizar arquivos.) Esse limite de largura de banda é uma taxa fixa em quilobytes por segundo. Quanto menor a largura de banda que a sincronização do OneDrive pode pegar, tanto mais lentamente o computador sincronizará os arquivos. A taxa mínima que pode ser definida é 1 KB/s, e a taxa máxima é de 100000 KB/s. Qualquer entrada inferior a 50 KB/s definirá o limite como 50 KB/s, mesmo que a interface do usuário mostre a taxa inserida.

Se você habilitar esta configuração, os computadores afetados por esta política usarão a largura de banda de download máxima que você especificar.

Se você desabilitar esta configuração, os computadores permitirão que os usuários determinem quanta largura de banda de download eles poderão usar.</string>
      <!-- turn off remote access/fetch on the computer (32-bit) -->
      <string id="RemoteAccessGPOEnabled">Impedir que os usuários usem o recurso de busca de arquivo remoto para acessar os arquivos no computador</string>
      <string id="RemoteAccessGPOEnabled_help">Esta configuração permite que você impeça que os usuários usem o recurso de busca quando eles estiverem conectados ao OneDrive.exe com sua conta da Microsoft. O recurso de busca permite que os usuários acessem o OneDrive.com, selecionem um computador do Windows que esteja online executando o cliente de sincronização do OneDrive no momento e acessem todos os seus arquivos pessoais a partir desse computador. Por padrão, os usuários podem usar o recurso de busca.

Se você habilitar esta configuração, os usuários não poderão usar o recurso de busca.

Se você desabilitar esta configuração, os usuários poderão usar o recurso de busca.

Esta configuração destina-se a computadores que executam versões de 32 bits ou 64 bits do Windows.</string>

      <!-- prevent OneDrive sync client (OneDrive.exe) from generating network traffic (checking for updates, etc.) until the user signs in to OneDrive -->
      <string id="PreventNetworkTrafficPreUserSignIn">Impedir que o OneDrive gere tráfego de rede até que o usuário entre no OneDrive</string>
      <string id="PreventNetworkTrafficPreUserSignIn_help">Habilite esta configuração para impedir que o cliente de sincronização do OneDrive (OneDrive.exe) gere tráfego de rede (verificação de atualizações etc.) até que o usuário entre no OneDrive ou inicie a sincronização de arquivos com o computador local.

Se você habilitar esta configuração, os usuários deverão entrar no cliente de sincronização do OneDrive no computador local ou escolher a sincronização automática dos arquivos do OneDrive ou do SharePoint no computador, para o cliente de sincronização iniciar automaticamente.

Se esta configuração não estiver habilitada, o cliente de sincronização do OneDrive será iniciado automaticamente quando os usuários entrarem no Windows.

Se você habilitar ou desabilitar esta configuração, não retorne a configuração para Não Configurado. Isso não alterará a configuração, e a última configuração permanecerá em vigor.</string>

      <!-- Silent Account Config -->
      <string id="SilentAccountConfig">Configurar silenciosamente o OneDrive usando a conta principal do Windows</string>
      <string id="SilentAccountConfig_help">Esta configuração permite que você configure o OneDrive silenciosamente usando a conta principal do Windows. 

Se você habilitar esta configuração, o OneDrive tentará entrar no OneDrive for Business usando essas credenciais. O OneDrive verificará o espaço em disco antes de sincronizar e, se houver bastante espaço, o OneDrive solicitará que o usuário escolha suas pastas. O limite para o qual o usuário é solicitado pode ser configurado usando-se o DiskSpaceCheckThresholdMB. O OneDrive tentará entrar em todas as contas do computador e, assim que o conseguir, a conta não tentará mais a configuração silenciosa.

Se você habilitar essa configuração, a ADAL deverá ser habilitada ou a configuração da conta falhará.

Se você habilitar esta configuração e o usuário estiver usando o cliente de sincronização do OneDrive for Business herdado, o novo cliente tentará assumir a sincronização do cliente herdado. Se ele conseguir, o OneDrive persistirá nas configurações de sincronização do usuário a partir do cliente herdado.

Se você desabilitar esta configuração, o OneDrive não tentará conectar os usuários automaticamente.

Outras configurações que são úteis com o SilentAccountConfig incluem o DiskSpaceCheckThresholdMB e o DefaultRootDir.
      </string>

      <!-- DiskSpaceCheckThresholdMB -->
      <string id="DiskSpaceCheckThresholdMB">O tamanho máximo do OneDrive for Business de um usuário antes que ele seja solicitado a escolher as pastas a serem baixadas</string>
      <string id="DiskSpaceCheckThresholdMB_help">Esta configuração é usada em conjunto com o SilentAccountConfig. Qualquer usuário que tenha um OneDrive for Business que seja maior do que o limite especificado (em MB) será solicitado a escolher as pastas que gostaria de sincronizar antes de o OneDrive baixar os arquivos.
      </string>

      <!-- Settings below control behavior of Files-On-Demand (Cloud Files) -->
      <string id="FilesOnDemandEnabled">Habilitar arquivos do OneDrive sob demanda</string>
      <string id="FilesOnDemandEnabled_help">Esta configuração permite controlar explicitamente se os Arquivos do OneDrive sob Demanda estão habilitados para seu locatário.

Se você habilitar esta configuração, os Arquivos do OneDrive sob Demanda serão ATIVADOS por padrão para todos os usuários aos quais a política se aplica.

Se você desabilitar esta configuração, os Arquivos do OneDrive sob Demanda serão desabilitados explicitamente e um usuário não poderá ativá-lo.

Se você não definir esta configuração, os Arquivos do OneDrive sob Demanda poderão ser ativados ou desativados por um usuário.
      </string>
      
      <string id="DehydrateSyncedTeamSites">Migrar Sites de Equipe Pré-existentes com Arquivos do OneDrive Sob Demanda</string>
      <string id="DehydrateSyncedTeamSites_help">Esta política aplica-se caso os Arquivos do OneDrive sob Demanda estejam habilitados.

Esta política permite que você migre o conteúdo do site de equipe baixado anteriormente para que seja somente online.

Se você habilitar esta diretiva, os sites de equipe que estavam sincronizando antes da habilitação dos Arquivos do OneDrive sob Demanda passarão a ser somente online por padrão.
      
Isso é ideal nos casos em que você deseja economizar largura de banda e possui muitos PCs sincronizando o mesmo site de equipe.
      </string>      
      
      <string id="AllowTenantList">Permitir a sincronização de contas do OneDrive somente para organizações específicas</string>
      <string id="AllowTenantList_help">Esta configuração permite que você impeça os usuários de carregarem arquivos facilmente para outras organizações, especificando uma lista de IDs de locatário permitidas. 

Se habilitar essa configuração, os usuários receberão um erro se tentarem adicionar uma conta de uma organização que não é permitida. Se um usuário já tiver adicionado a conta, a sincronização dos arquivos será interrompida.

Se desabilitar ou não definir essa configuração, os usuários poderão adicionar contas de qualquer organização. 

Para bloquear organizações específicas, use "Bloquear a sincronização de contas do OneDrive para organizações específicas".

Esta configuração tem prioridade sobre a política "Bloquear a sincronização de contas do OneDrive para organizações específicas". Não habilite ambas as políticas ao mesmo tempo.
      </string>
      
      <string id="BlockTenantList">Bloquear a sincronização de contas do OneDrive para organizações específicas</string>
      <string id="BlockTenantList_help">
Esta configuração permite que você impeça os usuários de carregarem arquivos facilmente para outra organização, especificando uma lista de IDs de locatário bloqueadas. 

Se habilitar essa configuração, os usuários receberão um erro se tentarem adicionar uma conta de uma organização que está bloqueada. Se um usuário já tiver adicionado a conta, a sincronização dos arquivos será interrompida.

Se desabilitar ou não definir essa configuração, os usuários poderão adicionar contas de qualquer organização. 

Para especificar uma lista de organizações permitidas, use "Permitir a sincronização de contas do OneDrive somente para organizações específicas".

Essa configuração NÃO funcionará se a política "Permitir a sincronização de contas do OneDrive somente para organizações específicas" estiver habilitada. Não habilite ambas as políticas ao mesmo tempo.
    </string>

    <!-- SharePoint On-Prem front door URL -->
    <string id="SharePointOnPremFrontDoorUrl">URL do servidor local do SharePoint e nome da pasta do locatário</string>
    <string id="SharePointOnPremFrontDoorUrl_help">Esta configuração de política permite que você defina uma URL do servidor Local do SharePoint. A URL é necessária para que os usuários possam sincronizar o OneDrive for Business hospedado no local. O nome do locatário é necessário para fornecer um esquema de nomeação apropriado para a pasta raiz.

Se você habilitar essa configuração e fornecer a URL local do SharePoint, bem como o nome da pasta do locatário, os usuários poderão sincronizar o OneDrive for Business deles hospedado no local.

Se você desabilitar ou não configurar a URL frontdoor ou o nome da pasta do locatário, os usuários não poderão sincronizar o OneDrive for Business deles hospedado no local.
    </string>

    <!-- SharePoint on-Prem prioritization settings -->
    <string id="SharePointOnPremPrioritization">Configuração de priorização do SharePoint para clientes híbridos que usam o SPO (SharePoint Online) e o servidor local do SharePoint</string>
    <string id="SharePointOnPremPrioritization_help">Essa configuração de política permite configurar onde o cliente de sincronização do OneDrive deve procurar o site pessoal do OneDrive for Business de um usuário (Meu Site) em um ambiente híbrido depois que um usuário estiver conectado.

Para usar esta configuração, você deve configurar a política de grupo da URL do servidor local do SharePoint. Essa configuração afeta somente a funcionalidade de sincronização do OneDrive for Business. Os usuários ainda poderão sincronizar os sites de equipe no SPO ou no SharePoint local independentemente dessa configuração.

Se você habilitar essa configuração, será possível selecionar uma das duas opções:

PrioritizeSPO: o cliente de sincronização examinará o SPO antes do servidor local do SharePoint do site pessoal do OneDrive for Business de um usuário. Se o cliente de sincronização já estiver configurado com o SPO para o usuário conectado, ele tentará configurar uma instância do OneDrive for Business do SharePoint local para esse usuário.

PrioritizeSharePointOnPrem: o cliente de sincronização examinará o servidor local do SharePoint antes do SPO do site pessoal do OneDrive for Business de um usuário. Se o cliente de sincronização já estiver configurado com o servidor local do SharePoint para o usuário conectado, ele tentará configurar uma instância do OneDrive for Business do SPO para esse usuário.

Se você desabilitar essa configuração, o comportamento será equivalente a opção PrioritizeSPO.
    </string>
    <string id="PrioritizeSPO">Priorize a sincronização do site pessoal do OneDrive for Business de um usuário com o SPO</string>
    <string id="PrioritizeSharePointOnPrem">Priorize a sincronização do site pessoal do OneDrive for Business de um usuário com o servidor local do SharePoint</string>
    
    <!-- Disable tutorial in the FRE -->
    <string id="DisableFRETutorial">Impedir que os usuários vejam o tutorial na Experiência de Entrada do OneDrive</string>
    <string id="DisableFRETutorial_help">Esta configuração permite impedir aos usuários de iniciarem o tutorial do navegador da Web no final da Tela de Apresentação do OneDrive

Se você habilitar essa configuração, os usuários que entrarem não verão o tutorial no final da Experiência de Entrada.

Se você desabilitar essa configuração, os usuários seguirão o comportamento original. Desabilitar tem o mesmo efeito que não definir essa configuração</string>
      <!-- Block KFM -->
      <string id="BlockKnownFolderMove">Impedir que os usuários movam as pastas conhecidas do Windows para o OneDrive</string>
      <string id="BlockKnownFolderMove_help">Esta configuração impede os usuários de mover as pastas Área de Trabalho, Imagens e Documentos para contas do OneDrive for Business.
Observação: a movimentação de pastas conhecidas para contas pessoais do OneDrive já está bloqueada em computadores que fazem parte de um domínio.

Se você habilitar essa configuração, os usuários não receberão a solicitação com a janela "Configurar a proteção de pastas importantes" e o comando "Iniciar Proteção" será desabilitado. Se o usuário já tiver movido as pastas conhecidas dele, os arquivos nessas pastas permanecerão no OneDrive. Esta política não terá efeito se você habilitar as opções "Solicitar aos usuários que movam as pastas conhecidas do Windows para o OneDrive" ou "Redirecionar silenciosamente as pastas conhecidas do Windows para o OneDrive".

Se você desabilitar ou não definir essa configuração, os usuários poderão optar por mover as pastas conhecidas. 
    </string>
    <!-- KFMOptInWithWizard -->
    <string id="KFMOptInWithWizard">Solicitar aos usuários que movam pastas conhecidas do Windows para o OneDrive </string>
    <string id="KFMOptInWithWizard_help">Esta configuração exibe a janela "Configurar a proteção de pastas importantes" que solicita aos usuários mover as pastas Área de Trabalho, Imagens e Documentos para contas do OneDrive. 

Quando você habilita essa configuração e fornece a ID do locatário, os usuários que sincronizam o OneDrive exibem a janela "Configurar a proteção de pastas importantes" ao se conectar. Se o usuário fechar a janela, nosso sistema exibirá um lembrete no centro de atividades, até que ele mova as três pastas conhecidas. Caso o usuário já tenha redirecionado as pastas conhecidas deles para outra conta do OneDrive, ele será solicitado a direcioná-las para a conta da sua organização (deixando os arquivos existentes para trás).

Se você desabilitar ou não configurar esta opção, a janela "Configurar a proteção de pastas importantes" não será exibida automaticamente para os usuários. 
    </string>
    <!-- KFMOptInNoWizard -->
    <string id="KFMOptInNoWizard">Redirecionar silenciosamente pastas conhecidas do Windows para o OneDrive</string>
    <string id="KFMOptInNoWizard_help">Esta configuração permite redirecionar as pastas Área de Trabalho, Imagens e Documentos para o OneDrive, sem interação dos usuários. Essa política se aplica quando todas as pastas conhecidas estão vazias e em pastas redirecionadas a uma conta diferente do OneDrive. Recomendamos usar essa política com a opção "Solicitar aos usuários mover pastas conhecidas do Windows para o OneDrive".
 
Quando você habilita essa política, as versões futuras não verificarão mais as pastas conhecidas vazias. Em vez disso, as pastas conhecidas serão redirecionadas e o conteúdo delas será movido.
 
Se habilitar essa configuração e fornecer a ID do locatário, você poderá optar por exibir uma notificação para os usuários quando as pastas deles forem redirecionadas.

Se você desabilitar ou não definir essa configuração, as pastas conhecidas dos usuários não serão redirecionadas para o OneDrive. 
    </string>
    <string id="KFMOptInNoWizardToast">Sim</string>
    <string id="KFMOptInNoWizardNoToast">Não</string>
     <!-- Block KFM Opt Out -->
      <string id="KFMBlockOptOut">Impedir os usuários de redirecionar pastas conhecidas do Windows para os respectivos computadores</string>
      <string id="KFMBlockOptOut_help">Esta configuração força os usuários a manter as pastas Área de Trabalho, Imagens e Documentos no OneDrive.
      
Se você habilitar essa configuração, o botão "Parar proteção", na janela "Configurar a proteção de pastas importantes", será desabilitado e os usuários receberão uma mensagem de erro quando tentarem interromper a sincronização de uma pasta conhecida.

Se você desabilitar ou não definir essa configuração, os usuários podem optar por redirecionar as pastas conhecidas para o computador. 
    </string>
    <string id="AutoMountTeamSites">Configurar as bibliotecas de site de equipe para sincronizarem automaticamente</string>
    <string id="AutoMountTeamSites_help">Essa configuração permite que você especifique as bibliotecas de site de equipe do SharePoint a sincronizadas automaticamente na próxima vez que os usuários entrarem para o cliente de sincronização do OneDrive (OneDrive.exe). Para usar a configuração, é necessário habilitar os Arquivos do OneDrive Sob Demanda e a configuração se aplica somente aos usuários em computadores com o Windows 10 Fall Creators Update ou posterior. Este recurso não está habilitado para os sites locais do SharePoint.
 
Se você habilitar essa configuração, o cliente de sincronização do OneDrive baixará automaticamente o conteúdo das bibliotecas especificadas como arquivos somente online na próxima vez que o usuário entrar. O usuário não poderá interromper a sincronização das bibliotecas. 
 
Se você desabilitar essa configuração, as bibliotecas de site de equipe especificadas não serão sincronizadas automaticamente para novos usuários. Os usuários existentes podem optar por parar a sincronização das bibliotecas, mas a sincronização das bibliotecas não será interrompida automaticamente.</string>
    <!-- Insert multi-tenant settings here -->
    <!-- See http://go.microsoft.com/fwlink/p/?LinkId=797547 for configuration instructions -->

    </stringTable>
    <presentationTable>
      <presentation id="AutomaticUploadBandwidthPercentage_Pres">
        <text>Selecione a porcentagem máxima de largura de banda a ser pega ao fazer upload de arquivos.</text>
        <text>Os valores válidos são de 10 a 99.</text>
        <decimalTextBox refId="BandwidthSpinBox" defaultValue="70" spinStep="1">Largura de banda:</decimalTextBox>
      </presentation>

      <presentation id="UploadBandwidthLimit_Pres">
        <text>Selecione a quantidade máxima de largura de banda a ser pega ao fazer upload de arquivos.</text>
        <text>Os valores válidos são de 1 a 100000.</text>
        <decimalTextBox refId="UploadRateValue" defaultValue="125">Largura de banda:</decimalTextBox>
      </presentation>

      <presentation id="DownloadBandwidthLimit_Pres">
        <text>Selecione a quantidade máxima de largura de banda a ser pega ao fazer download de arquivos.</text>
        <text>Os valores válidos são de 1 a 100000.</text>
        <decimalTextBox refId="DownloadRateValue" defaultValue="125">Largura de banda:</decimalTextBox>
      </presentation>

       <presentation id="DiskSpaceCheckThresholdMB_Pres">
        <text>Especifique o GUID do locatário e o tamanho máximo do OneDrive for Business de um usuário antes que o usuário seja solicitado a escolher as pastas que deseja sincronizar. </text>
        <text>No campo Nome, digite o GUID do locatário. Digite o tamanho no campo Valor.</text>
        <text>Os valores válidos são de 0 a 4294967295 MB (inclusive).</text>
        <listBox refId="DiskSpaceCheckThresholdMBList">Caminhos de locatário: </listBox>
      </presentation>

      <presentation id="DefaultRootDir_Pres">
       <text>Especifique o GUID do locatário e o caminho padrão. </text>
        <text>No campo Nome, digite o GUID do locatário. Digite o caminho no campo Valor.</text>
        <listBox refId="DefaultRootDirList">Caminhos de locatário: </listBox>
      </presentation>
      
      <presentation id="DisableCustomRoot_Pres">
        <text>Especifique o GUID do locatário e o valor da configuração. 1 para ativar esta configuração, 0 para desativar esta configuração </text>
        <text>No campo Nome, digite o GUID do locatário. No campo Valor, digite 1 ou 0.</text>
        <listBox refId="DisableCustomRootList">Caminhos de locatário: </listBox>
      </presentation>
      
      <presentation id="AllowTenantList_Pres">
        <text>Especificar a ID do locatário</text>
        <text>No campo valor, digite a ID do locatário que deseja adicionar à lista</text>
        <text> </text>
        <listBox refId="AllowTenantListBox">GUID do Locatário: </listBox>
      </presentation>
      
      <presentation id="BlockTenantList_Pres">
        <text>Especificar a ID do locatário</text>
        <text>No campo valor, digite a ID do locatário que deseja adicionar à lista</text>
        <text> </text>
        <listBox refId="BlockTenantListBox">GUID do Locatário: </listBox>
       </presentation>

      <presentation id="SharePointOnPremFrontDoorUrl_Pres">
        <text>Forneça uma URL para o servidor local do SharePoint que hospede o OneDrive for Business do usuário e o nome da pasta do locatário.</text>
        <textBox refId="SharePointOnPremFrontDoorUrlBox">
          <label>URL do servidor local do SharePoint:</label>
        </textBox>
        <textBox refId="SharePointOnPremTenantNameBox">
          <label>Nome da Pasta do Locatário:</label>
        </textBox>
      </presentation>

      <presentation id="SharePointOnPremPrioritization_Pres">
        <dropdownList refId="SharePointOnPremPrioritization_Dropdown" noSort="true" defaultItem="0">Configuração de priorização do SharePoint para clientes híbridos</dropdownList>
      </presentation>
      
      <presentation id="BlockKnownFolderMove_Pres">
        <dropdownList refId="BlockKnownFolderMove_Dropdown" noSort="true" defaultItem="0">Caso as pastas conhecidas já tenham sido movidas para o OneDrive:</dropdownList>
      </presentation>
      
      <presentation id="KFMOptInWithWizard_Pres">
        <textBox refId="KFMOptInWithWizard_TextBox">
          <label>ID do Locatário:</label>
        </textBox>
      </presentation> 
      
      <presentation id="KFMOptInNoWizard_Pres">
        <textBox refId="KFMOptInNoWizard_TextBox">
          <label>ID do Locatário:</label>
        </textBox>
        <dropdownList refId="KFMOptInNoWizard_Dropdown" noSort="true" defaultItem="0">Mostrar notificação aos usuários quando as pastas forem redirecionadas:</dropdownList>
      </presentation>
      <presentation id="AutoMountTeamSites_Pres">
        <text>Para especificar uma biblioteca para sincronizar:

Abra um navegador da Web, entre no Office 365 como um administrador do SharePoint ou global da sua organização e vá para a biblioteca. 

Clique no botão de Sincronizar na biblioteca que deseja sincronizar automaticamente e, em seguida, clique em "Copiar ID da biblioteca." 

Clique em "Mostrar" para inserir a ID da biblioteca com um identificador no campo nome.
        </text>
        <listBox refId="AutoMountTeamSitesListBox">Bibliotecas</listBox>
      </presentation>
    </presentationTable>
  </resources>
</policyDefinitionResources>
