<?xml version="1.0" encoding="utf-8"?>
<!-- (c) 2016 Microsoft Corporation -->
<policyDefinitionResources xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" revision="1.0" schemaVersion="1.0" xmlns="http://www.microsoft.com/GroupPolicy/PolicyDefinitions">
  <displayName>Paramètres de stratégie de groupe OneDrive</displayName>
  <description>Divers paramètres de stratégie de groupe pour le client de synchronisation OneDrive, en particulier ceux pour la configuration de paramètres spécifiques aux fonctionnalités de l’entreprise dans le client.</description>
  <resources>
    <stringTable>
      <!-- general -->
      <string id="OneDriveNGSCSettingCategory">OneDrive</string>
      
      <!-- block syncing personal OneDrive -->
      <string id="DisablePersonalSync">Empêcher les utilisateurs de synchroniser leur compte OneDrive personnel</string>
      <string id="DisablePersonalSync_help">Ce paramètre vous permet d’empêcher les utilisateurs de synchroniser les fichiers avec la version grand public de OneDrive (basée sur un compte Microsoft). Par défaut, les utilisateurs ne peuvent pas synchroniser les comptes OneDrive personnels.

Si vous activez ce paramètre, les utilisateurs ne peuvent pas configurer une relation de synchronisation pour leur compte OneDrive personnel. S’ils ont précédemment établi une relation de synchronisation avec un compte OneDrive personnel, ils reçoivent une erreur lorsqu’ils démarrent le client de synchronisation, mais leurs fichiers restent sur le disque.

Si vous désactivez ce paramètre, les utilisateurs peuvent synchroniser les comptes OneDrive personnels.</string>

      <!-- turn on enterprise tier cadence for app updates -->
      <string id="EnableEnterpriseUpdate">Nous avons rencontré un délai lors de la mise à jour de OneDrive.exe jusqu’à la deuxième vague de mise à jour</string>
      <string id="EnableEnterpriseUpdate_help">La mise à jour de OneDrive.exe s’effectue en deux phases. La première phase commence quand une mise à jour devient disponible ; cette opération ne prend généralement qu’une semaine ou deux. La deuxième phase commence une fois que la première phase est terminée.

Ce paramètre empêche les clients de synchronisation OneDrive d’être mis à jour jusqu’à la deuxième phase. Vous aurez ainsi assez de temps pour vous préparer à la mise à jour. 

Par défaut, les mises à jour sont installées dès qu’elles sont disponibles lors de la première phase.

Si vous activez ce paramètre, les clients de synchronisation OneDrive dans votre domaine seront mis à jour au cours de la deuxième phase, plusieurs semaines après que les mises à jour soient publiées en disponibilité générale auprès des clients Office 365.

Si vous désactivez ce paramètre, les clients de synchronisation OneDrive seront mis à jour dès que des mises à jour seront disponibles au cours de la deuxième phase.</string>

      <!-- set default location of the OneDrive folder -->
      <string id="DefaultRootDir">Définir l’emplacement par défaut pour le dossier OneDrive</string>
      <string id="DefaultRootDir_help">Ce paramètre vous permet de définir un chemin d’accès spécifique en tant qu’emplacement par défaut du dossier OneDrive lorsque des utilisateurs suivent l’Assistant Bienvenue dans OneDrive lors de la configuration de la synchronisation des fichiers. Par défaut, le chemin d’accès se trouve sous %userprofile%.

Si vous activez ce paramètre, l’emplacement de dossier OneDrive - {tenant name} est réinitialisé sur l’emplacement que vous spécifiez dans le fichier OneDrive.admx.

Si vous désactivez ce paramètre, l’emplacement du dossier OneDrive - {tenant name} sera remplacé par %userprofile%.</string>

      <!-- disable changing the default location of the OneDrive folder -->
      <string id="DisableCustomRoot">Empêcher les utilisateurs de modifier l’emplacement de leur dossier OneDrive</string>
      <string id="DisableCustomRoot_help">Ce paramètre vous permet d’empêcher les utilisateurs de modifier l’emplacement de leur dossier de synchronisation OneDrive.

Si vous activez ce paramètre, les utilisateurs ne peuvent pas modifier l’emplacement de leur dossier OneDrive - {tenant name} lors de l’Assistant Bienvenue dans OneDrive. Cela force les utilisateurs à utiliser l’emplacement par défaut, ou, si vous avez défini le paramètre « Définir l’emplacement par défaut pour le dossier OneDrive », cela vous assure que tous les utilisateurs disposent de leur dossier OneDrive local dans leurs emplacement que vous avez spécifié.

Si vous désactivez ce paramètre, les utilisateurs peuvent modifier l’emplacement de leur dossier de synchronisation lors de l’Assistant Bienvenue dans OneDrive.</string>

      <!-- Enable Office Integration for coauthoring and in-app sharing -->
      <string id="EnableAllOcsiClients">Co-édition et partage dans l’application pour les fichiers Office</string>
      <string id="EnableAllOcsiClients_help">Ce paramètre autorise le partage dans l’application et la co-édition dynamique pour les fichiers Office ouverts localement à partir de votre ordinateur. La co-édition et le partage dans l’application pour les fichiers Office sont autorisés par défaut. (La co-édition est disponible dans Office 2013 et Office 2016.)

Si vous activez ce paramètre, la co-édition et le partage dans l’application pour Office sont activés, mais les utilisateurs peuvent désactiver ces fonctionnalités à tout moment dans l’onglet Office du client de synchronisation.

Si vous désactivez ce paramètre, la co-édition et le partage dans l’application pour les fichiers Office sont désactivés, et l’onglet Office est masqué dans le client de synchronisation. Si vous désactivez ce paramètre, le paramètre « Les utilisateurs peuvent choisir le mode de traitement des fichiers Office en conflit » sera considéré comme étant désactivé et, en cas de conflits de fichiers, le fichier sera dupliqué.</string>


      <!-- Enable hold the file for handling Office conflicts -->
      <string id="EnableHoldTheFile">Les utilisateurs peuvent choisir le mode de traitement des fichiers Office en conflit</string>
      <string id="EnableHoldTheFile_help">Ce paramètre détermine ce qui se produit lorsqu’un conflit survient entre des versions de fichiers Office 2016 pendant la synchronisation. Par défaut, les utilisateurs sont autorisés à décider s’ils souhaitent fusionner les modifications ou conserver les deux copies. Les utilisateurs peuvent également configurer le client de synchronisation de manière à toujours dupliquer le fichier et à conserver les deux copies. (Cette option est uniquement disponible pour Office 2016. Avec les versions antérieures d’Office, le fichier est toujours dupliqué et les deux copies sont conservées.)

Si vous activez ce paramètre, les utilisateurs peuvent décider s’ils souhaitent fusionner les modifications ou conserver les deux copies. Les utilisateurs peuvent également configurer le client de synchronisation de manière à ce qu’il duplique toujours le fichier et conserve les deux copies.

Si vous désactivez ce paramètre, le fichier est toujours dupliqué et les deux fichiers sont conservés en cas de conflit de synchronisation. Le paramètre de synchronisation dans le client de synchronisation est désactivé.</string>

      <!-- Enable Automatic Upload Bandwidth Limiting -->
      <string id="AutomaticUploadBandwidthPercentage">Définir le pourcentage maximal de bande passante de chargement utilisé par OneDrive.exe</string>
      <string id="AutomaticUploadBandwidthPercentage_help">Ce paramètre vous permet de configurer le pourcentage maximal de bande passante de chargement disponible sur l’ordinateur que la synchronisation OneDrive utilise lors du chargement. (OneDrive utilise uniquement cette bande passante lors de la synchronisation de fichiers.) La bande passante disponible sur un ordinateur change constamment ; par conséquent, un pourcentage autorise la synchronisation à répondre aux augmentations et aux diminutions de la disponibilité en bande passante lors de la synchronisation en arrière-plan. Plus le pourcentage de bande-passante que la synchronisation OneDrive est autorisée à utiliser est faible, plus l’ordinateur mettra du temps à synchroniser les fichiers. Nous vous recommandons d’utiliser un pourcentage supérieur ou égal à 50 %. La synchronisation autorise la limitation du chargement en autorisant régulièrement le moteur de synchronisation à passer en vitesse maximale pendant une minute, puis à diminuer le pourcentage de chargement jusqu’à la valeur définie par ce paramètre. Deux principaux scénarios se dégagent. Tout d’abord, un fichier de très petite taille est chargé rapidement, car il peut être chargé dans l’intervalle pendant lequel la synchronisation calcule la vitesse maximale possible. Ensuite, pour un chargement long, la synchronisation optimise constamment la vitesse de chargement conformément à la valeur de pourcentage définie par ce paramètre.

Si vous activez ce paramètre, les ordinateurs concernés par cette stratégie utilisent le pourcentage de bande passante que vous spécifiez.

Si vous désactivez ce paramètre, les ordinateurs autorisent les utilisateurs à déterminer la quantité de bande passante de chargement qu’ils peuvent utiliser.

Si vous activez ou désactivez ce paramètre, ne redéfinissez pas le paramètre sur Non configuré. Si vous agissez de la sorte, cela ne modifiera pas la configuration et le dernier paramètre configuré restera actif.</string>

      <!-- Enable Upload Bandwidth Limiting -->
      <string id="UploadBandwidthLimit">Définir la bande passante maximale de chargement utilisée par OneDrive.exe</string>
      <string id="UploadBandwidthLimit_help">Ce paramètre vous permet de configurer la bande passante maximale disponible sur l’ordinateur qu’utilise la synchronisation OneDrive dans le cadre du chargement. (OneDrive utilise uniquement cette bande passante lors de la synchronisation de fichiers.) Cette limite de bande passante est un taux fixe en kilo-octets par seconde. Plus la bande passante que la synchronisation OneDrive est autorisée à utiliser est faible, plus l’ordinateur mettra du temps à synchroniser les fichiers. Le taux minimal pouvant être défini est de 1 Ko/s et le taux maximal est de 100 000 Ko/s. Toute entrée inférieure à 50 Ko/s définit la limite à 50 Ko/s, même si l’interface utilisateur affiche le taux entré.

Si vous activez ce paramètre, les ordinateurs concernés par cette stratégie utilisent la bande passante maximale de chargement que vous spécifiez.

Si vous désactivez ce paramètre, les ordinateurs autorisent les utilisateurs à déterminer la quantité de bande passante de chargement qu’ils peuvent utiliser.</string>

      <!-- Enable Download Bandwidth Limiting -->
      <string id="DownloadBandwidthLimit">Définir la bande passante maximale de chargement utilisée par OneDrive.exe</string>
      <string id="DownloadBandwidthLimit_help">Ce paramètre vous permet de configurer la bande passante maximale disponible sur l’ordinateur qu’utilise la synchronisation OneDrive dans le cadre du téléchargement. (OneDrive utilise uniquement cette bande passante lors de la synchronisation de fichiers.) Cette limite de bande passante est un taux fixe en kilo-octets par seconde. Plus la bande passante que la synchronisation OneDrive est autorisée à utiliser est faible, plus l’ordinateur mettra du temps à synchroniser les fichiers. Le taux minimal pouvant être défini est de 1 Ko/s et le taux maximal est de 100 000 Ko/s. Toute entrée inférieure à 50 Ko/s définit la limite à 50 Ko/s, même si l’interface utilisateur affiche le taux entré.

Si vous activez ce paramètre, les ordinateurs concernés par cette stratégie utilisent la bande passante maximale de téléchargement que vous spécifiez.

Si vous désactivez ce paramètre, les ordinateurs autorisent les utilisateurs à déterminer la quantité de bande passante de téléchargement qu’ils peuvent utiliser.</string>
      <!-- turn off remote access/fetch on the computer (32-bit) -->
      <string id="RemoteAccessGPOEnabled">Empêcher les personnes d’utiliser la fonctionnalité de récupération de fichiers à distance pour accéder aux fichiers sur l’ordinateur</string>
      <string id="RemoteAccessGPOEnabled_help">Ce paramètre vous permet d’empêcher les personnes d’utiliser la fonctionnalité de récupération lorsqu’ils sont connectés à OneDrive.exe avec leur compte Microsoft. La fonctionnalité de récupération autorise vos utilisateurs à accéder à OneDrive.com, à sélectionner un ordinateur Windows actuellement en ligne et à exécuter le client de synchronisation OneDrive, et à accéder à tous leurs fichiers personnels à partir de cet ordinateur. Par défaut, les personnes peuvent utiliser la fonctionnalité de récupération.

Si vous activez ce paramètre, les personnes ne peuvent pas utiliser la fonctionnalité de récupération.

Si vous désactivez ce paramètre, les personnes peuvent utiliser la fonctionnalité de récupération.

Ce paramètre s’applique aux ordinateurs exécutant la version 32 bits ou 64 bits de Windows.</string>

      <!-- prevent OneDrive sync client (OneDrive.exe) from generating network traffic (checking for updates, etc.) until the user signs in to OneDrive -->
      <string id="PreventNetworkTrafficPreUserSignIn">Empêcher OneDrive de générer du trafic réseau tant que l’utilisateur n’est pas connecté à OneDrive</string>
      <string id="PreventNetworkTrafficPreUserSignIn_help">Activez ce paramètre pour empêcher le client de synchronisation OneDrive (OneDrive.exe) de générer du trafic réseau (vérification des mises à jour, etc.) tant que l’utilisateur n’est pas connecté à OneDrive ou n’a pas démarré la synchronisation de fichiers sur l’ordinateur local.

Si vous activez ce paramètre, les utilisateurs doivent se connecter au client de synchronisation OneDrive sur l’ordinateur local ou choisir de synchroniser des fichiers OneDrive ou SharePoint sur l’ordinateur pour que le client de synchronisation démarre automatiquement.

Si ce paramètre n’est pas activé, le client de synchronisation OneDrive démarre automatiquement lorsque les utilisateurs ouvrent une session Windows.

Si vous activez ou désactivez ce paramètre, ne redéfinissez pas le paramètre sur Non configuré. Si vous agissez de la sorte, cela ne modifiera pas la configuration et le dernier paramètre configuré restera actif.</string>

      <!-- Silent Account Config -->
      <string id="SilentAccountConfig">Configurer OneDrive en mode silencieux à l’aide du compte Windows principal</string>
      <string id="SilentAccountConfig_help">Ce paramètre vous permet de configurer OneDrive en mode silencieux à l’aide du compte Windows principal. 

Si vous activez ce paramètre, OneDrive tente de se connecter à OneDrive Entreprise à l’aide de ces informations d’identification. OneDrive vérifie l’espace disponible sur le disque avant de procéder à la synchronisation et si l’espace est suffisant, OneDrive invite l’utilisateur à choisir ses dossiers. Le seuil auquel l’utilisateur reçoit l’invite peut être configuré à l’aide de DiskSpaceCheckThresholdMB. OneDrive tente de se connecter à chaque compte sur l’ordinateur et une fois l’opération effectuée, le compte ne tente plus d’effectuer une configuration en mode silencieux.

Si vous activez ce paramètre, ADAL doit être activé ou la configuration du compte échoue.

Si vous activez ce paramètre et que la personne utilise le client de synchronisation OneDrive Entreprise hérité, le nouveau client tente de reprendre la synchronisation à partir du client hérité. Si l’opération réussi, OneDrive rendra les paramètres de synchronisation de l’utilisateur persistants à partir du client hérité.

Si vous désactivez ce paramètre, OneDrive ne tente pas de connecter automatiquement les utilisateurs.

Les autres paramètres utiles avec SilentAccountConfig sont DiskSpaceCheckThresholdMB et DefaultRootDir.
      </string>

      <!-- DiskSpaceCheckThresholdMB -->
      <string id="DiskSpaceCheckThresholdMB">Taille maximale de l’espace OneDrive Entreprise d’un utilisateur avant qu’il ne soit invité à choisir les dossiers qui sont téléchargés</string>
      <string id="DiskSpaceCheckThresholdMB_help">Ce paramètre est utilisé conjointement avec SilentAccountConfig. Tout utilisateur dont l’espace OneDrive Entreprise est supérieur au seuil spécifié (en Mo) est invité à choisir les dossiers qu’il souhaite synchroniser avant que OneDrive ne télécharge les fichiers.
      </string>

      <!-- Settings below control behavior of Files-On-Demand (Cloud Files) -->
      <string id="FilesOnDemandEnabled">Activer les fichiers à la demande OneDrive</string>
      <string id="FilesOnDemandEnabled_help">Ce paramètre vous permet de contrôler de manière explicite si les fichiers à la demande OneDrive sont activés pour votre client.

Si vous activez ce paramètre, les fichiers à la demande OneDrive sont ACTIVÉS par défaut pour tous les utilisateurs auxquels la stratégie s’applique.

Si vous désactivez ce paramètre, les fichiers à la demande OneDrive sont désactivés de manière explicite et un utilisateur ne peut pas les désactiver.

Si vous ne configurez pas ce paramètre, les fichiers à la demande OneDrive peuvent être activés ou désactivés par un utilisateur.
      </string>
      
      <string id="DehydrateSyncedTeamSites">Migrer les sites d’équipe préexistants avec les fichiers à la demande OneDrive</string>
      <string id="DehydrateSyncedTeamSites_help">Cette stratégie s’applique si les fichiers à la demande OneDrive sont activés.

Cette stratégie vous permet de migrer le contenu des sites d’équipe téléchargés précédemment via le mode En ligne uniquement.

Si vous activez cette stratégie, les sites d’équipe qui étaient synchronisés avant que les fichiers à la demande OneDrive ne soient synchronisés passeront par défaut en mode En ligne uniquement.
      
Cette stratégie est idéale quand vous souhaitez conserver de la bande passante et qu’un grand nombre de PC synchronisent le même site d’équipe.
      </string>      
      
      <string id="AllowTenantList">Autoriser la synchronisation des comptes OneDrive uniquement pour des organisations spécifiques</string>
      <string id="AllowTenantList_help">Ce paramètre vous permet d’empêcher les utilisateurs de charger des fichiers facilement vers d’autres organisations en spécifiant une liste d’ID de clients autorisés. 

Si vous activez ce paramètre, les utilisateurs reçoivent une erreur s’ils tentent d’ajouter un compte à partir d’une organisation non autorisée. Si un utilisateur a déjà ajouté le compte, les fichiers arrêtent de se synchroniser.

Si vous désactivez ce paramètre ou si vous ne le configurez pas, les utilisateurs peuvent ajouter des comptes à partir de n’importe quelle organisation. 

Au lieu de cela, pour bloquer des organisations spécifiques, utilisez la stratégie « Bloquer la synchronisation des comptes OneDrive pour des organisations spécifiques ».

Ce paramètre est prioritaire sur la stratégie « Bloquer la synchronisation des comptes OneDrive pour des organisations spécifiques ». N’activez pas les deux stratégies en même temps.
      </string>
      
      <string id="BlockTenantList">Bloquer la synchronisation des comptes OneDrive pour des organisations spécifiques</string>
      <string id="BlockTenantList_help">
Ce paramètre vous permet d’empêcher des utilisateurs de charger des fichiers facilement vers une autre organisation en spécifiant une liste d’ID de clients bloqués. 

Si vous activez ce paramètre, les utilisateurs reçoivent une erreur s’ils tentent d’ajouter un compte à partir d’une organisation bloquée. Si un utilisateur a déjà ajouté le compte, les fichiers arrêtent de se synchroniser.

Si vous désactivez ce paramètre ou si vous ne le configurez pas, les utilisateurs peuvent ajouter des comptes à partir de n’importe quelle organisation. 

Au lieu de cela, pour spécifier une liste d’organisations autorisées, utilisez la stratégie « Autoriser la synchronisation des comptes OneDrive uniquement pour des organisations spécifiques ».

Ce paramètre ne fonctionne PAS si vous avez activé la stratégie « Autoriser la synchronisation des comptes OneDrive uniquement pour des organisations spécifiques ». N’activez pas les deux stratégies en même temps.
    </string>

    <!-- SharePoint On-Prem front door URL -->
    <string id="SharePointOnPremFrontDoorUrl">URL du serveur local SharePoint et nom de dossier du client</string>
    <string id="SharePointOnPremFrontDoorUrl_help">Ce paramètre de stratégie vous permet de définir une URL de serveur local SharePoint. L’URL est requise pour que l’utilisateur puisse effectuer une synchronisation avec le service OneDrive Entreprise hébergé localement. Le nom du client est requis pour fournir un schéma d’affectation de nom approprié pour le dossier racine.

Si vous activez ce paramètre et fournissez l’URL locale SharePoint, ainsi que le nom de dossier du client, les utilisateurs pourront synchroniser leur service OneDrive Entreprise hébergé localement.

Si vous désactivez ou omettez de configurer l’URL principale ou le nom de dossier du client, les utilisateurs ne pourront pas synchroniser leur service OneDrive Entreprise hébergé localement.
    </string>

    <!-- SharePoint on-Prem prioritization settings -->
    <string id="SharePointOnPremPrioritization">Paramètre de définition des priorités SharePoint pour les clients hybrides qui utilisent SharePoint Online (SPO) et un serveur SharePoint local</string>
    <string id="SharePointOnPremPrioritization_help">Ce paramètre de stratégie vous permet de configurer l’emplacement dans lequel le client de synchronisation OneDrive doit rechercher le site personnel (Mon site) OneDrive Entreprise d’un utilisateur dans un environnement hybride lorsqu’un utilisateur se connecte.

Pour utiliser ce paramètre, vous devez configurer la stratégie de groupe URL du serveur SharePoint local. Ce paramètre ne concerne que la fonctionnalité de synchronisation de OneDrive Entreprise. Les utilisateurs peuvent toujours synchroniser des sites d’équipe sur SPO ou SharePoint en local, quel que soit la valeur de ce paramètre.

Si vous activez ce paramètre, vous pouvez sélectionner l’une des deux options suivantes :

PrioritizeSPO : le client de synchronisation recherche le site personnel OneDrive Entreprise d’un utilisateur sur SPO avant de rechercher sur le serveur SharePoint local. Si le client de synchronisation est déjà configuré avec SPO pour l’utilisateur connecté, il tente de configurer une instance OneDrive Entreprise sur le serveur SharePoint local pour cet utilisateur.

PrioritizeSharePointOnPrem : le client de synchronisation recherche le site personnel OneDrive Entreprise d’un utilisateur sur le serveur SharePoint local avant de rechercher sur SPO. Si le client de synchronisation est déjà configuré avec le serveur SharePoint local pour l’utilisateur connecté, il tente de configurer une instance OneDrive Entreprise sur SPO pour cet utilisateur.

Si vous désactivez ce paramètre, cela revient à utiliser l’option PrioritizeSPO.
    </string>
    <string id="PrioritizeSPO">Donner la priorité à la synchronisation du site personnel OneDrive Entreprise d’un utilisateur avec SharePoint Online</string>
    <string id="PrioritizeSharePointOnPrem">Donner la priorité à la synchronisation du site personnel OneDrive Entreprise d’un utilisateur avec le serveur SharePoint local</string>
    
    <!-- Disable tutorial in the FRE -->
    <string id="DisableFRETutorial">Empêcher les utilisateurs de voir le didacticiel dans l’expérience de connexion OneDrive</string>
    <string id="DisableFRETutorial_help">Ce paramètre vous permet d’empêcher les utilisateurs de lancer le didacticiel à la fin de l’introduction de l’interface de OneDrive lors de la première utilisation

Si vous activez ce paramètre, les utilisateurs qui se connectent ne verront pas le didacticiel à la fin de l’expérience de connexion.

Si vous désactivez ce paramètre, les utilisateurs suivront le comportement d’origine. Le fait de désactiver ce paramètre revient à ne pas le configurer.</string>
      <!-- Block KFM -->
      <string id="BlockKnownFolderMove">Empêcher les utilisateurs de déplacer leurs dossiers Windows connus vers OneDrive</string>
      <string id="BlockKnownFolderMove_help">Ce paramètre empêche les utilisateurs de déplacer leurs dossiers Documents, Images et Bureau vers un compte OneDrive Entreprise.
Remarque : la possibilité de déplacer des dossiers connus vers des comptes OneDrive personnels est déjà bloquée sur des PC joints à des domaines.

Si vous activez ce paramètre, la fenêtre « Configuration de la protection des dossiers importants » n’est pas affichée aux utilisateurs et la commande « Démarrer la protection » est désactivée. Si l’utilisateur a déjà déplacé ses dossiers connus, les fichiers contenus dans ces dossiers resteront dans OneDrive. Cette stratégie ne s’applique pas si vous avez activé le paramètre « Inviter les utilisateurs à déplacer des dossiers Windows connus vers OneDrive » ou « Rediriger de manière silencieuse les dossiers Windows connus vers OneDrive ».

Si vous désactivez ce paramètre ou si vous ne le configurez pas, les utilisateurs peuvent choisir de déplacer leurs dossiers connus. 
    </string>
    <!-- KFMOptInWithWizard -->
    <string id="KFMOptInWithWizard">Inviter les utilisateurs à déplacer des dossiers Windows connus vers OneDrive</string>
    <string id="KFMOptInWithWizard_help">Ce paramètre permet d’afficher la fenêtre « Configuration de la protection des dossiers importants » qui invite les utilisateurs à déplacer leur dossier Documents, Images et Bureau vers OneDrive. 

Si vous activez ce paramètre et fournissez votre ID de client, les utilisateurs qui synchronisent leur espace OneDrive verront la fenêtre « Configuration de la protection des dossiers importants » lorsqu’ils se connectent. S’ils ferment la fenêtre, une notification de rappel s’affichera dans le Centre d’activités jusqu’à ce qu’ils déplacent les trois dossiers connus. Si un utilisateur a déjà redirigé ses dossiers connus vers un autre compte OneDrive, il est invité à diriger les dossiers vers le compte de votre organisation (en ignorant les fichiers existants).

Si vous désactivez ce paramètre ou si vous ne le configurez pas, la fenêtre « Configuration de la protection des dossiers importants » ne s’affiche pas automatiquement à vos utilisateurs. 
    </string>
    <!-- KFMOptInNoWizard -->
    <string id="KFMOptInNoWizard">Rediriger de manière silencieuse les dossiers Windows connus vers OneDrive</string>
    <string id="KFMOptInNoWizard_help">Ce paramètre vous permet de rediriger les dossiers Documents, Images et Bureau de vos utilisateurs vers OneDrive sans interaction de la part des utilisateurs. Cette stratégie fonctionne lorsque tous les dossiers connus sont vides et sur les dossiers redirigés vers un compte OneDrive différent. Nous vous conseillons d’utiliser cette stratégie conjointement avec le paramètre « Inviter les utilisateurs à déplacer des dossiers Windows connus vers OneDrive ».
 
Lorsque vous activez cette stratégie, les prochaines versions ne rechercheront plus les dossiers vides connus. Les dossiers connus seront redirigés et leur contenu déplacé.
 
Si vous activez ce paramètre et fournissez votre ID de client, vous pouvez choisir d’afficher une notification aux utilisateurs après que leurs dossiers ont été redirigés.

Si vous désactivez ce paramètre ou si vous ne le configurez pas, les dossiers connus de vos utilisateurs ne seront pas redirigés de manière silencieuse vers OneDrive. 
    </string>
    <string id="KFMOptInNoWizardToast">Oui</string>
    <string id="KFMOptInNoWizardNoToast">Non</string>
     <!-- Block KFM Opt Out -->
      <string id="KFMBlockOptOut">Empêcher les utilisateurs de rediriger leurs dossiers Windows connus vers leur PC</string>
      <string id="KFMBlockOptOut_help">Ce paramètre force les utilisateurs à conserver leur dossier Documents, Images et Bureau qui pointent vers OneDrive.
      
Si vous activez ce paramètre, le bouton « Arrêter la protection » de la fenêtre « Configuration de la protection des dossiers importants » est désactivé et les utilisateurs reçoivent une erreur s’ils tentent d’arrêter la synchronisation d’un dossier connu.

Si vous désactivez ce paramètre ou si vous ne le configurez pas, les utilisateurs peuvent choisir de rediriger leurs dossiers connus vers leur PC. 
    </string>
    <string id="AutoMountTeamSites">Configurer les bibliothèques de sites d’équipe à synchroniser automatiquement</string>
    <string id="AutoMountTeamSites_help">Ce paramètre vous permet de spécifier les bibliothèques de sites d’équipe SharePoint à synchroniser automatiquement la prochaine fois que des utilisateurs se connectent au client de synchronisation OneDrive (OneDrive.exe). Pour utiliser le paramètre, vous devez activer les fichiers à la demande OneDrive. De plus, le paramètre ne s’applique qu’aux personnes qui utilisent des ordinateurs exécutant Windows 10 Fall Creators Update ou version ultérieure. Cette fonctionnalité n’est pas activée pour les sites SharePoint locaux.
 
Si vous activez ce paramètre, le client de synchronisation OneDrive téléchargera automatiquement le contenu des bibliothèques que vous spécifierez en tant que fichiers en ligne uniquement la prochaine fois que l’utilisateur se connectera. L’utilisateur ne peut pas arrêter la synchronisation des bibliothèques. 
 
Si vous désactivez ce paramètre, les bibliothèques de sites d’équipe que vous avez spécifiées ne sont pas synchronisées automatiquement pour les nouveaux utilisateurs. Les utilisateurs existants peuvent choisir d’interrompre la synchronisation des bibliothèques, mais la synchronisation de ces dernières ne sera pas interrompue automatiquement.</string>
    <!-- Insert multi-tenant settings here -->
    <!-- See http://go.microsoft.com/fwlink/p/?LinkId=797547 for configuration instructions -->

    </stringTable>
    <presentationTable>
      <presentation id="AutomaticUploadBandwidthPercentage_Pres">
        <text>Sélectionnez le pourcentage maximal de bande passante à utiliser lors du chargement de fichiers.</text>
        <text>Les valeurs valides sont comprises entre 10 et 99.</text>
        <decimalTextBox refId="BandwidthSpinBox" defaultValue="70" spinStep="1">Bande passante :</decimalTextBox>
      </presentation>

      <presentation id="UploadBandwidthLimit_Pres">
        <text>Sélectionnez la quantité maximale de bande passante à utiliser lors du chargement de fichiers.</text>
        <text>Les valeurs valides sont comprises entre 1 et 100 000.</text>
        <decimalTextBox refId="UploadRateValue" defaultValue="125">Bande passante :</decimalTextBox>
      </presentation>

      <presentation id="DownloadBandwidthLimit_Pres">
        <text>Sélectionnez la quantité maximale de bande passante à utiliser lors du téléchargement de fichiers.</text>
        <text>Les valeurs valides sont comprises entre 1 et 100 000.</text>
        <decimalTextBox refId="DownloadRateValue" defaultValue="125">Bande passante :</decimalTextBox>
      </presentation>

       <presentation id="DiskSpaceCheckThresholdMB_Pres">
        <text>Spécifiez le GUID du client et la taille maximale de l’espace OneDrive Entreprise d’un utilisateur avant que ce dernier ne soit invité à choisir les dossiers qu’il souhaite synchroniser. </text>
        <text>Dans le champ Nom, entrez le GUID du client. Dans le champ Valeur, entrez la taille.</text>
        <text>Les valeurs valides sont comprises entre 0 et 4 294 967 295 Mo (inclus).</text>
        <listBox refId="DiskSpaceCheckThresholdMBList">Chemins d’accès aux clients : </listBox>
      </presentation>

      <presentation id="DefaultRootDir_Pres">
       <text>Spécifiez le GUID du client et le chemin d’accès par défaut. </text>
        <text>Dans le champ Nom, entrez le GUID du client. Dans le champ Valeur, entrez le chemin d’accès.</text>
        <listBox refId="DefaultRootDirList">Chemins d’accès aux clients : </listBox>
      </presentation>
      
      <presentation id="DisableCustomRoot_Pres">
        <text>Spécifiez le GUID du client et la valeur de ce paramètre : 1 pour activer ce paramètre et 0 pour le désactiver </text>
        <text>Dans le champ Nom, entrez le GUID du client. Dans le champ Valeur, entrez 1 ou 0.</text>
        <listBox refId="DisableCustomRootList">Chemins d’accès aux clients : </listBox>
      </presentation>
      
      <presentation id="AllowTenantList_Pres">
        <text>Spécifier l’ID du client</text>
        <text>Dans le champ Valeur, entrez l’ID du client que vous souhaitez ajouter à cette liste</text>
        <text> </text>
        <listBox refId="AllowTenantListBox">GUID du client : </listBox>
      </presentation>
      
      <presentation id="BlockTenantList_Pres">
        <text>Spécifier l’ID du client</text>
        <text>Dans le champ Valeur, entrez l’ID du client que vous souhaitez ajouter à cette liste</text>
        <text> </text>
        <listBox refId="BlockTenantListBox">GUID du client : </listBox>
       </presentation>

      <presentation id="SharePointOnPremFrontDoorUrl_Pres">
        <text>Indiquez une URL vers le serveur SharePoint local qui héberge le site OneDrive Entreprise de l’utilisateur, et le nom de dossier du client.</text>
        <textBox refId="SharePointOnPremFrontDoorUrlBox">
          <label>URL du serveur SharePoint local :</label>
        </textBox>
        <textBox refId="SharePointOnPremTenantNameBox">
          <label>Nom de dossier du client :</label>
        </textBox>
      </presentation>

      <presentation id="SharePointOnPremPrioritization_Pres">
        <dropdownList refId="SharePointOnPremPrioritization_Dropdown" noSort="true" defaultItem="0">Paramètre de définition des priorités SharePoint pour les clients hybrides</dropdownList>
      </presentation>
      
      <presentation id="BlockKnownFolderMove_Pres">
        <dropdownList refId="BlockKnownFolderMove_Dropdown" noSort="true" defaultItem="0">Si des dossiers connus ont déjà été déplacés vers OneDrive :</dropdownList>
      </presentation>
      
      <presentation id="KFMOptInWithWizard_Pres">
        <textBox refId="KFMOptInWithWizard_TextBox">
          <label>ID de client :</label>
        </textBox>
      </presentation> 
      
      <presentation id="KFMOptInNoWizard_Pres">
        <textBox refId="KFMOptInNoWizard_TextBox">
          <label>ID de client :</label>
        </textBox>
        <dropdownList refId="KFMOptInNoWizard_Dropdown" noSort="true" defaultItem="0">Afficher une notification aux utilisateurs après que des dossiers ont été redirigés :</dropdownList>
      </presentation>
      <presentation id="AutoMountTeamSites_Pres">
        <text>Pour spécifier une bibliothèque à synchroniser :

Ouvrez un navigateur web, connectez-vous à Office 365 en tant qu’administrateur général ou administrateur SharePoint de votre organisation, puis accédez à la bibliothèque. 

Cliquez sur le bouton Synchroniser dans la bibliothèque que vous souhaitez synchroniser automatiquement, puis cliquez sur « Copier l’ID de bibliothèque ». 

Cliquez sur « Afficher » pour entrer l’ID de la bibliothèque conjointement avec un identificateur dans le champ de nom.
        </text>
        <listBox refId="AutoMountTeamSitesListBox">Bibliothèques</listBox>
      </presentation>
    </presentationTable>
  </resources>
</policyDefinitionResources>
