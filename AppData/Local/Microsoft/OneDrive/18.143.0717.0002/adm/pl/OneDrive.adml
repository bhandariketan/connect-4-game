<?xml version="1.0" encoding="utf-8"?>
<!-- (c) 2016 Microsoft Corporation -->
<policyDefinitionResources xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" revision="1.0" schemaVersion="1.0" xmlns="http://www.microsoft.com/GroupPolicy/PolicyDefinitions">
  <displayName>Ustawienia zasad grupy usługi OneDrive</displayName>
  <description>Różne ustawienia zasad grupy dla klienta synchronizacji OneDrive, zwłaszcza do konfigurowania ustawień specyficznych dla funkcji przedsiębiorstwa w kliencie.</description>
  <resources>
    <stringTable>
      <!-- general -->
      <string id="OneDriveNGSCSettingCategory">OneDrive</string>
      
      <!-- block syncing personal OneDrive -->
      <string id="DisablePersonalSync">Uniemożliwianie użytkownikom synchronizowania osobistych kont usługi OneDrive</string>
      <string id="DisablePersonalSync_help">To ustawienie pozwala na zablokowanie użytkownikom możliwości synchronizowania plików z usługi OneDrive konsumenta (na podstawie konta Microsoft). Domyślnie użytkownicy mogą synchronizować osobiste konta usługi OneDrive.

Jeśli włączysz to ustawienie, użytkownicy nie będą mogli skonfigurować relacji synchronizacji dla ich osobistego konta usługi OneDrive. Jeśli wcześniej synchronizowali osobiste konto usługi OneDrive, zostanie im wyświetlony błąd, gdy uruchomią klienta synchronizacji, ale ich pliki pozostaną na dysku.

Jeśli wyłączysz to ustawienie, użytkownicy będą mogli synchronizować osobiste konta usługi OneDrive.</string>

      <!-- turn on enterprise tier cadence for app updates -->
      <string id="EnableEnterpriseUpdate">Opóźnienie aktualizacji aplikacji OneDrive.exe do drugiego etapu wydania</string>
      <string id="EnableEnterpriseUpdate_help">Aktualizacja usługi OneDrive.exe jest przeprowadzana w dwóch etapach. Pierwszy etap rozpoczyna się, gdy aktualizacja staje się dostępna i zwykle jej ukończenie trwa od jednego do dwóch tygodni. Drugi etap rozpoczyna się po ukończeniu pierwszego.

To ustawienie zapobiega aktualizowaniu klientów synchronizacji OneDrive do czasu drugiego etapu. Daje to nieco dodatkowego czasu na przygotowanie się do nadchodzących aktualizacji.

Domyślnie aktualizacje są instalowane, gdy tylko staną się dostępne podczas pierwszego etapu.

Jeśli włączysz to ustawienie, klienci synchronizacji OneDrive w Twojej domenie zostaną zaktualizowani podczas drugiego etapu, kilka tygodni po udostępnieniu aktualizacji szerokiemu gronu klientów usługi Office 365.

Jeśli wyłączysz to ustawienie, klienci synchronizacji usługi OneDrive zostaną zaktualizowani, gdy tylko aktualizacje staną się dostępne podczas pierwszego etapu.</string>

      <!-- set default location of the OneDrive folder -->
      <string id="DefaultRootDir">Ustawianie domyślnej lokalizacji folderu usługi OneDrive</string>
      <string id="DefaultRootDir_help">To ustawienie pozwala ustawić określoną ścieżkę jako domyślną lokalizację folderu usługi OneDrive, gdy użytkownicy przechodzą przez kroki kreatora OneDrive — Zapraszamy! podczas konfigurowania synchronizacji plików. Domyślnie ścieżka znajduje się w zmiennej %userprofile%.

Jeśli włączysz to ustawienie, lokalizacja lokalnego folderu usługi OneDrive — {tenant name} będzie ustawiana domyślnie na ścieżkę określoną przez Ciebie w pliku OneDrive.admx.

Jeśli wyłączysz to ustawienie, lokalizacja lokalnego folderu usługi OneDrive — {tenant name} będzie ustawiana domyślnie na ścieżkę ze zmiennej %userprofile%.</string>

      <!-- disable changing the default location of the OneDrive folder -->
      <string id="DisableCustomRoot">Uniemożliwianie użytkownikom zmiany lokalizacji folderu usługi OneDrive</string>
      <string id="DisableCustomRoot_help">To ustawienie pozwala uniemożliwić użytkownikom zmienianie lokalizacji ich folderu synchronizacji usługi OneDrive.

Jeśli włączysz to ustawienie, użytkownicy nie będą mogli zmieniać lokalizacji ich folderu usługi OneDrive — {tenant name} podczas działania kreatora OneDrive — Zapraszamy!. To zmusi użytkowników do używania lokalizacji domyślnej lub, jeśli skonfigurowano ustawienie „Ustawianie domyślnej lokalizacji folderu usługi OneDrive”, zapewni, że wszyscy użytkownicy będą mieli lokalny folder usługi OneDrive w lokalizacji określonej przez Ciebie.

Jeśli wyłączysz to ustawienie, użytkownicy będą mogli zmieniać lokalizację ich folderu synchronizacji podczas działania kreatora OneDrive — Zapraszamy!.</string>

      <!-- Enable Office Integration for coauthoring and in-app sharing -->
      <string id="EnableAllOcsiClients">Współtworzenie plików pakietu Office i udostępnianie ich w aplikacji</string>
      <string id="EnableAllOcsiClients_help">To ustawienie umożliwia współtworzenie na żywo plików pakietu Office otwartych lokalnie z komputera oraz udostępnianie ich w aplikacji. Domyślnie współtworzenie plików pakietu Office i udostępnianie ich w aplikacji jest dozwolone. (Współtworzenie jest dostępne w pakietach Office 2013 i Office 2016).

Jeśli włączysz to ustawienie, współtworzenie i udostępnianie w aplikacji dla pakietu Office będzie włączone, ale użytkownicy będą mogli w razie potrzeby wyłączyć je na karcie Office w kliencie synchronizacji.

Jeśli wyłączysz to ustawienie, współtworzenie plików pakietu Office i udostępnianie ich w aplikacji będzie wyłączone, a karta Office będzie ukryta w kliencie synchronizacji. Jeśli wyłączysz to ustawienie, wówczas ustawienie „Umożliwianie użytkownikom wyboru sposobu obsługi plików pakietu Office w przypadku konfliktu” będzie zachowywać się jak wyłączone i w razie konfliktów między plikami plik zostanie rozwidlony.</string>


      <!-- Enable hold the file for handling Office conflicts -->
      <string id="EnableHoldTheFile">Umożliwianie użytkownikom wyboru sposobu obsługi plików pakietu Office w przypadku konfliktu</string>
      <string id="EnableHoldTheFile_help">To ustawienie określa, co się stanie, gdy wystąpi konflikt między wersjami plików pakietu Office 2016 podczas synchronizacji. Domyślnie użytkownicy mogą decydować, czy chcą scalić zmiany, czy zachować obie kopie. Użytkownicy mogą również skonfigurować klienta synchronizacji tak, aby zawsze rozwidlał plik i zachowywał obie kopie. (Ta opcja jest dostępna tylko dla pakietu Office 2016. We wcześniejszych wersjach pakietu Office plik jest zawsze rozwidlany i obie kopie są zachowywane).

Jeśli włączysz to ustawienie, użytkownicy będą mogli decydować, czy chcą scalić zmiany, czy zachować obie kopie. Użytkownicy będą mogli również skonfigurować klienta synchronizacji tak, aby zawsze rozwidlał plik i zachowywał obie kopie.

Jeśli wyłączysz to ustawienie, plik będzie zawsze rozwidlany i będą zachowywane obie kopie na wypadek konfliktu synchronizacji. Ustawienie konfiguracji na kliencie synchronizacji jest wyłączone.</string>

      <!-- Enable Automatic Upload Bandwidth Limiting -->
      <string id="AutomaticUploadBandwidthPercentage">Ustawianie maksymalnej wartości procentowej przepustowości przekazywania używanej przez aplikację OneDrive.exe</string>
      <string id="AutomaticUploadBandwidthPercentage_help">To ustawienie pozwala skonfigurować maksymalny procent dostępnej przepustowości na komputerze, której synchronizacja usługi OneDrive będzie używać do przekazywania. (Usługa OneDrive używa tej przepustowości tylko podczas synchronizowania plików). Przepustowość dostępna dla komputera stale się zmienia, więc wartość procentowa pozwala procesowi synchronizacji reagować zarówno na wzrosty, jak i spadki dostępności przepustowości podczas synchronizowania w tle. Im niższy procent przepustowości może zająć synchronizacja usługi OneDrive, tym wolniej komputer będzie synchronizować pliki. Zalecamy wartość 50% lub wyższą. Synchronizacja umożliwia ograniczanie przekazywania przez okresowe zezwalanie aparatowi synchronizacji na przejście do pełnej szybkości przez jedną minutę, a następnie zwolnienie do procentu przekazywania wyznaczonego przez to ustawienie. Pozwala to na dwa kluczowe scenariusze. W pierwszym bardzo mały plik zostanie szybko przekazany, ponieważ może zmieścić się w interwale, w którym synchronizacja osiąga maksymalną możliwą szybkość. W drugim dla każdego długo trwającego przekazywania synchronizacja będzie stale optymalizować szybkość przekazywania według wartości procentowej wyznaczonej przez to ustawienie.

Jeśli włączysz to ustawienie, komputery, do których są stosowane te zasady, będą używać maksymalnego procentu przepustowości określonego przez Ciebie.

Jeśli wyłączysz to ustawienie, komputery zezwolą użytkownikom na określenie ilości przepustowości przekazywania, której mogą użyć.

Jeśli włączysz lub wyłączysz to ustawienie, nie przywracaj ustawienia do wartości Nieskonfigurowane. Zrobienie tego nie spowoduje zmiany w konfiguracji i nadal będzie obowiązywać ostatnie skonfigurowane ustawienie.</string>

      <!-- Enable Upload Bandwidth Limiting -->
      <string id="UploadBandwidthLimit">Ustawianie maksymalnej przepustowości przekazywania używanej przez aplikację OneDrive.exe</string>
      <string id="UploadBandwidthLimit_help">To ustawienie pozwala skonfigurować maksymalną dostępną przepustowość na komputerze, której synchronizacja usługi OneDrive będzie używać do przekazywania. (Usługa OneDrive używa tej przepustowości tylko podczas synchronizowania plików). Ten limit przepustowości to stała szybkość w kilobajtach na sekundę. Im niższą przepustowość może zająć synchronizacja usługi OneDrive, tym wolniej komputer będzie synchronizować pliki. Minimalna szybkość, jaką można ustawić, to 1 KB/s, a maksymalna to 100000 KB/s. Każda wartość wejściowa niższa niż 50 KB/s spowoduje ustawienie limitu na 50 KB/s, nawet jeśli interfejs użytkownika pokazuje wprowadzoną szybkość.

Jeśli włączysz to ustawienie, komputery, do których są stosowane te zasady, będą używać maksymalnej przepustowości przekazywania określonej przez Ciebie.

Jeśli wyłączysz to ustawienie, komputery zezwolą użytkownikom na określenie ilości przepustowości przekazywania, której mogą użyć.</string>

      <!-- Enable Download Bandwidth Limiting -->
      <string id="DownloadBandwidthLimit">Ustawianie maksymalnej przepustowości pobierania używanej przez aplikację OneDrive.exe</string>
      <string id="DownloadBandwidthLimit_help">To ustawienie pozwala skonfigurować maksymalną dostępną przepustowość na komputerze, której synchronizacja usługi OneDrive będzie używać do pobierania. (Usługa OneDrive używa tej przepustowości tylko podczas synchronizowania plików). Ten limit przepustowości to stała szybkość w kilobajtach na sekundę. Im niższą przepustowość może zająć synchronizacja usługi OneDrive, tym wolniej komputer będzie synchronizować pliki. Minimalna szybkość, jaką można ustawić, to 1 KB/s, a maksymalna to 100000 KB/s. Każda wartość wejściowa niższa niż 50 KB/s spowoduje ustawienie limitu na 50 KB/s, nawet jeśli interfejs użytkownika pokazuje wprowadzoną szybkość.

Jeśli włączysz to ustawienie, komputery, do których są stosowane te zasady, będą używać maksymalnej przepustowości pobierania określonej przez Ciebie.

Jeśli wyłączysz to ustawienie, komputery zezwolą użytkownikom na określenie ilości przepustowości pobierania, której mogą użyć.</string>
      <!-- turn off remote access/fetch on the computer (32-bit) -->
      <string id="RemoteAccessGPOEnabled">Uniemożliwianie użytkownikom uzyskiwania dostępu do plików na komputerze przy użyciu funkcji zdalnego dostępu do plików</string>
      <string id="RemoteAccessGPOEnabled_help">To ustawienie pozwala zablokować użytkownikom możliwość korzystania z funkcji uzyskiwania zdalnego dostępu, gdy są zalogowani do aplikacji OneDrive.exe za pomocą konta Microsoft. Funkcja uzyskiwania zdalnego dostępu pozwala użytkownikom na przejście do witryny OneDrive.com, wybranie komputera z systemem Windows, który jest obecnie w trybie online i ma uruchomionego klienta synchronizacji OneDrive, i uzyskanie dostępu do wszystkich ich plików osobistych z tego komputera. Domyślnie użytkownicy mogą korzystać z funkcji uzyskiwania zdalnego dostępu.

Jeśli włączysz to ustawienie, użytkownicy nie będą mogli korzystać z funkcji uzyskiwania zdalnego dostępu.

Jeśli wyłączysz to ustawienie, użytkownicy będą mogli korzystać z funkcji uzyskiwania zdalnego dostępu.

To ustawienie dotyczy komputerów z 32-bitową lub 64-bitową wersją systemu Windows.</string>

      <!-- prevent OneDrive sync client (OneDrive.exe) from generating network traffic (checking for updates, etc.) until the user signs in to OneDrive -->
      <string id="PreventNetworkTrafficPreUserSignIn">Uniemożliwianie usłudze OneDrive generowania ruchu w sieci do czasu zalogowania się użytkownika do usługi OneDrive</string>
      <string id="PreventNetworkTrafficPreUserSignIn_help">Włącz to ustawienie, aby zapobiec generowaniu przez klienta synchronizacji OneDrive (OneDrive.exe) ruchu sieciowego (sprawdzanie aktualizacji itp.), dopóki użytkownik nie zaloguje się do usługi OneDrive lub nie rozpocznie synchronizowania plików na komputerze lokalnym.

Jeśli włączysz to ustawienie, użytkownicy będą musieli zalogować się do klienta synchronizacji OneDrive na komputerze lokalnym lub wybrać synchronizację plików usługi OneDrive lub programu SharePoint na komputerze, aby klient synchronizacji został uruchomiony automatycznie.

Jeśli to ustawienie nie będzie włączone, klient synchronizacji OneDrive zostanie uruchomiony automatycznie, gdy użytkownicy zalogują się do systemu Windows.

Jeśli włączysz lub wyłączysz to ustawienie, nie przywracaj ustawienia do wartości Nieskonfigurowane. Zrobienie tego nie spowoduje zmiany w konfiguracji i nadal będzie obowiązywać ostatnie skonfigurowane ustawienie.</string>

      <!-- Silent Account Config -->
      <string id="SilentAccountConfig">Dyskretne konfigurowanie usługi OneDrive przy użyciu podstawowego konta systemu Windows</string>
      <string id="SilentAccountConfig_help">To ustawienie pozwala skonfigurować usługę OneDrive w trybie dyskretnym przy użyciu podstawowego konta systemu Windows.

Jeśli włączysz to ustawienie, usługa OneDrive będzie próbować zalogować się do usługi OneDrive dla Firm przy użyciu tych poświadczeń. Usługa OneDrive sprawdzi ilość miejsca na dysku przed synchronizacją, a jeśli usługa OneDrive zajmuje dużo miejsca, wyświetli użytkownikowi monit o wybranie folderów. Próg, po osiągnięciu którego użytkownik będzie monitowany, można skonfigurować za pomocą ustawienia DiskSpaceCheckThresholdMB. Usługa OneDrive będzie podejmować próbę zalogowania się na każdym koncie na komputerze, a gdy próba zakończy się powodzeniem, dla danego konta nie będzie już podejmowana próba dyskretnej konfiguracji.

Jeśli włączysz to ustawienie, musi być włączona biblioteka ADAL. W przeciwnym razie konfiguracja konta zakończy się niepowodzeniem.

Jeśli włączysz to ustawienie, a użytkownik korzysta ze starszego klienta synchronizacji OneDrive dla Firm, nowy klient podejmie próbę przejęcia synchronizacji od starszej wersji klienta. Jeśli zakończy się ona powodzeniem, usługa OneDrive zachowa ustawienia synchronizacji użytkownika ze starszej wersji klienta.

Jeśli wyłączysz to ustawienie, usługa OneDrive nie podejmie próby automatycznego zalogowania użytkowników.

Inne ustawienia przydatne w przypadku trybu SilentAccountConfig to DiskSpaceCheckThresholdMB i DefaultRootDir.
      </string>

      <!-- DiskSpaceCheckThresholdMB -->
      <string id="DiskSpaceCheckThresholdMB">Maksymalny rozmiar usługi OneDrive dla Firm użytkownika przed wyświetleniem monitu o wybranie folderów do pobrania</string>
      <string id="DiskSpaceCheckThresholdMB_help">To ustawienie jest używane w połączeniu z ustawieniem SilentAccountConfig. Każdy użytkownik, który ma usługę OneDrive dla Firm o rozmiarze przekraczającym określony próg (w MB), będzie monitowany o wybranie folderów, które chce z synchronizować, zanim usługa OneDrive pobierze pliki.
      </string>

      <!-- Settings below control behavior of Files-On-Demand (Cloud Files) -->
      <string id="FilesOnDemandEnabled">Włączanie funkcji Pliki na żądanie usługi OneDrive</string>
      <string id="FilesOnDemandEnabled_help">To ustawienie pozwala na jawne kontrolowanie, czy funkcja Pliki na żądanie usługi OneDrive jest włączona dla dzierżawy.

Jeśli włączysz to ustawienie, funkcja Pliki na żądanie usługi OneDrive będzie domyślnie włączona dla wszystkich użytkowników, do których są stosowane zasady.

Jeśli wyłączysz to ustawienie, funkcja Pliki na żądanie usługi OneDrive zostanie jawnie wyłączona i użytkownik nie będzie mógł jej włączyć.

Jeśli nie skonfigurujesz tego ustawienia, funkcja Pliki na żądanie usługi OneDrive będzie mogła być włączana lub wyłączana przez użytkownika.
    </string>
      
      <string id="DehydrateSyncedTeamSites">Migrowanie istniejących wcześniej witryn zespołu za pomocą funkcji Pliki na żądanie usługi OneDrive</string>
      <string id="DehydrateSyncedTeamSites_help">Te zasady są stosowane, jeśli funkcja Pliki na żądanie usługi OneDrive jest włączona.

Te zasady umożliwiają zmigrowanie pobranej wcześniej zawartości witryny zespołu do zawartości tylko w trybie online.

Jeśli włączysz te zasady, witryny zespołu, które były synchronizowane przed włączeniem funkcji Pliki na żądanie usługi OneDrive, zostaną domyślnie przekształcone na zawartość Tylko w trybie online.

Jest to idealne rozwiązanie w przypadkach, gdy chcesz zaoszczędzić przepustowość i umożliwić wielu komputerom synchronizowanie tej samej witryny zespołu.
</string>      
      
      <string id="AllowTenantList">Zezwalanie na synchronizację kont usługi OneDrive dla określonych organizacji</string>
      <string id="AllowTenantList_help">Za pomocą tego ustawienia możesz uniemożliwić użytkownikom łatwe przekazywanie plików do innych organizacji, określając listę dozwolonych identyfikatorów dzierżaw.

Jeśli włączysz to ustawienie, użytkownikom będzie zwracany błąd przy próbie dodania konta z organizacji, która jest niedozwolona. Jeśli użytkownik dodał już konto, synchronizacja plików zostanie zatrzymana.

Jeśli wyłączysz to ustawienie lub nie skonfigurujesz go, użytkownicy będą mogli dodawać konta z dowolnej organizacji.

Aby zamiast tego zablokować określone organizacje, użyj ustawienia „Blokowanie synchronizacji kont usługi OneDrive dla określonych organizacji”.

To ustawienie ma priorytet względem zasad „Blokowanie synchronizacji kont usługi OneDrive dla określonych organizacji”. Nie włączaj obu tych zasad jednocześnie.
</string>
      
      <string id="BlockTenantList">Blokowanie synchronizacji kont usługi OneDrive dla określonych organizacji</string>
      <string id="BlockTenantList_help">
Za pomocą tego ustawienia możesz uniemożliwić użytkownikom łatwe przekazywanie plików do innej organizacji, określając listę zablokowanych identyfikatorów dzierżaw.

Jeśli włączysz to ustawienie, użytkownikom będzie zwracany błąd przy próbie dodania konta z organizacji, która jest zablokowana. Jeśli użytkownik dodał już konto, synchronizacja plików zostanie zatrzymana.

Jeśli wyłączysz to ustawienie lub nie skonfigurujesz go, użytkownicy będą mogli dodawać konta z dowolnej organizacji. 

Aby zamiast tego określić listę dozwolonych organizacji, użyj ustawienia „Zezwalanie na synchronizację kont usługi OneDrive tylko dla określonych organizacji”.

To ustawienie NIE będzie działać, jeśli są włączone zasady „Zezwalanie na synchronizację kont usługi OneDrive tylko dla określonych organizacji”. Nie włączaj obu tych zasad jednocześnie.
</string>

    <!-- SharePoint On-Prem front door URL -->
    <string id="SharePointOnPremFrontDoorUrl">Adres URL serwera lokalnego programu SharePoint i nazwa folderu dzierżawy</string>
    <string id="SharePointOnPremFrontDoorUrl_help">To ustawienie zasad pozwala ustawić adres URL serwera lokalnego programu SharePoint. Adres URL jest wymagany, aby użytkownik mógł synchronizować zawartość z usługą OneDrive dla Firm, która jest hostowana lokalnie. Nazwa dzierżawy jest wymagana do zapewnienia odpowiedniego schematu nazewnictwa dla folderu głównego.

Jeśli to ustawienie zostanie włączone oraz zostaną podane lokalny adres URL programu SharePoint i nazwa folderu dzierżawy, użytkownicy będą mogli synchronizować usługę OneDrive dla Firm, która jest hostowana lokalnie.

Jeśli adres URL lub nazwa folderu dzierżawy zostaną wyłączone lub nie zostaną skonfigurowane, użytkownicy nie będą mogli synchronizować usługi OneDrive dla Firm, która jest hostowana lokalnie.
    </string>

    <!-- SharePoint on-Prem prioritization settings -->
    <string id="SharePointOnPremPrioritization">Ustawienie priorytetyzacji w programie SharePoint dla klientów modelu hybrydowego korzystających z serwera lokalnego programu SharePoint i usługi SharePoint Online (SPO)</string>
    <string id="SharePointOnPremPrioritization_help">To ustawienie zasad pozwala skonfigurować lokalizację, w której klient synchronizacji OneDrive ma wyszukiwać witrynę osobistą użytkownika usługi OneDrive dla Firm (Moja witryna) w środowisku hybrydowym po zalogowaniu się użytkownika.

Aby użyć tego ustawienia, musisz skonfigurować zasady grupy dotyczące adresu URL lokalnego serwera programu SharePoint. To ustawienie dotyczy tylko funkcji synchronizacji usługi OneDrive dla Firm. Użytkownicy nadal będą mogli synchronizować witryny zespołu w usłudze SPO lub lokalnym programie SharePoint niezależnie od tego ustawienia.

Jeśli to ustawienie zostanie włączone, będzie można wybrać jedną z dwóch opcji:

PrioritizeSPO: Klient synchronizacji będzie wyszukiwał witrynę osobistą użytkownika usługi OneDrive dla Firm w usłudze SPO przed lokalnym serwerem programu SharePoint. Jeśli klient synchronizacji jest już skonfigurowany przy użyciu usługi SPO dla zalogowanego użytkownika, podejmie próbę skonfigurowania lokalnego wystąpienia usługi OneDrive dla Firm w programie SharePoint dla tego użytkownika.

PrioritizeSharePointOnPrem: Klient synchronizacji będzie wyszukiwał witrynę osobistą użytkownika usługi OneDrive dla Firm na lokalnym serwerze programu SharePoint przed usługą SPO. Jeśli klient synchronizacji jest już skonfigurowany przy użyciu lokalnego serwera programu SharePoint dla zalogowanego użytkownika, podejmie próbę skonfigurowania wystąpienia usługi OneDrive dla Firm w usłudze SPO dla tego użytkownika.

Jeśli to ustawienie zostanie wyłączone, zachowanie będzie równoważne opcji PrioritizeSPO.
    </string>
    <string id="PrioritizeSPO">Ustaw priorytet synchronizacji witryny osobistej usługi OneDrive dla Firm użytkownika dla usługi SPO</string>
    <string id="PrioritizeSharePointOnPrem">Ustaw priorytet synchronizacji witryny osobistej usługi OneDrive dla Firm użytkownika dla lokalnego serwera programu SharePoint</string>
    
    <!-- Disable tutorial in the FRE -->
    <string id="DisableFRETutorial">Uniemożliwiaj użytkownikom wyświetlanie samouczka w środowisku logowania usługi OneDrive</string>
    <string id="DisableFRETutorial_help">To ustawienie pozwala uniemożliwić użytkownikom uruchamianie samouczka przeglądarki internetowej pod koniec pierwszego uruchomienia usługi OneDrive

Jeśli włączysz to ustawienie, użytkownicy, którzy się zalogują, nie zobaczą samouczka pod koniec procesu logowania.

Jeśli wyłączysz to ustawienie, użytkownicy będą postępować zgodnie z pierwotnym zachowaniem. Wyłączenie ma taki sam skutek jak nieskonfigurowanie tego ustawienia</string>
      <!-- Block KFM -->
      <string id="BlockKnownFolderMove">Uniemożliwiaj użytkownikom przenoszenie znanych folderów systemu Windows do usługi OneDrive</string>
      <string id="BlockKnownFolderMove_help">To ustawienie uniemożliwia użytkownikom przenoszenie folderów Dokumenty, Obrazy i Pulpit na żadne konto usługi OneDrive dla Firm.
Uwaga: przenoszenie znanych folderów na osobiste konta usługi OneDrive jest już zablokowane na komputerach przyłączonych do domeny.

Jeśli to ustawienie zostanie włączone, u użytkowników nie będzie wyświetlane okno „Konfigurowanie ochrony ważnych folderów”, a polecenie „Rozpocznij ochronę” będzie wyłączone. Jeśli użytkownik przeniósł już swoje znane foldery, pliki w tych folderach pozostaną w usłudze OneDrive. Te zasady nie są stosowane, jeśli włączono ustawienie „Monituj użytkowników o przeniesienie znanych folderów systemu Windows do usługi OneDrive” lub „Dyskretnie przekierowuj znane foldery systemu Windows do usługi OneDrive”.

Jeśli to ustawienie zostanie wyłączone lub nie zostanie skonfigurowane, użytkownicy będą mogli przenosić znane foldery. 
    </string>
    <!-- KFMOptInWithWizard -->
    <string id="KFMOptInWithWizard">Monituj użytkowników o przeniesienie znanych folderów systemu Windows do usługi OneDrive </string>
    <string id="KFMOptInWithWizard_help">To ustawienie umożliwia wyświetlenie okna „Konfigurowanie ochrony ważnych folderów” z monitem dla użytkowników o przeniesienie folderów Dokumenty, Obrazy i Pulpit do usługi OneDrive. 

Jeśli to ustawienie zostanie włączone i zostanie podany identyfikator dzierżawy, u użytkowników synchronizujących usługę OneDrive po zalogowaniu się zostanie wyświetlone okno „Konfigurowanie ochrony ważnych folderów”. Jeśli użytkownicy zamkną okno, w centrum aktywności zostanie wyświetlone powiadomienie z przypomnieniem do czasu przeniesienia wszystkich trzech znanych folderów. Jeśli użytkownik już przekierował znane foldery na inne konto usługi OneDrive, zostanie wyświetlony monit o przekierowanie folderów na konto organizacji (i pozostawienie istniejących plików).

Jeśli to ustawienie zostanie wyłączone lub nie zostanie skonfigurowane, u użytkowników nie będzie automatycznie wyświetlane okno „Konfigurowanie ochrony ważnych folderów”. 
    </string>
    <!-- KFMOptInNoWizard -->
    <string id="KFMOptInNoWizard">Dyskretnie przekierowuj znane foldery systemu Windows do usługi OneDrive</string>
    <string id="KFMOptInNoWizard_help">To ustawienie umożliwia przekierowanie folderów użytkowników Dokumenty, Obrazy i Pulpit do usługi OneDrive bez interakcji z użytkownikiem. Te zasady są stosowane, gdy wszystkie znane foldery są puste i w przypadku folderów przekierowanych na inne konto usługi OneDrive. Zalecamy używanie tych zasad wraz z ustawieniem „Monituj użytkowników o przeniesienie znanych folderów systemu Windows do usługi OneDrive”.
 
Jeśli te zasady zostaną włączone, przyszłe wersje nie będą już sprawdzać pustych znanych folderów. Zamiast tego znane foldery zostaną przekierowane i ich zawartość zostanie przeniesiona.
 
Jeśli to ustawienie zostanie włączone i zostanie podany identyfikator dzierżawy, będzie można określić, czy u użytkowników ma być wyświetlane powiadomienie po przekierowaniu folderów.

Jeśli to ustawienie zostanie wyłączone lub nie zostanie skonfigurowane, znane foldery użytkowników nie będą dyskretnie przekierowywane do usługi OneDrive. 
    </string>
    <string id="KFMOptInNoWizardToast">Tak</string>
    <string id="KFMOptInNoWizardNoToast">Nie</string>
     <!-- Block KFM Opt Out -->
      <string id="KFMBlockOptOut">Uniemożliwiaj użytkownikom przekierowywanie znanych folderów systemu Windows na komputer</string>
      <string id="KFMBlockOptOut_help">To ustawienie wymusza na użytkownikach zachowanie folderów Dokumenty, Obrazy i Pulpit kierowanych do usługi OneDrive.
      
Jeśli to ustawienie zostanie włączone, przycisk „Zatrzymaj ochronę” w oknie „Konfigurowanie ochrony ważnych folderów” zostanie wyłączony i użytkownicy otrzymają komunikat o błędzie w przypadku próby zatrzymania synchronizacji znanego folderu.

Jeśli to ustawienie zostanie wyłączone lub nie zostanie skonfigurowane, użytkownicy będą mogli ponownie przekierować znane foldery na komputer. 
    </string>
    <string id="AutoMountTeamSites">Skonfiguruj automatyczną synchronizację bibliotek witryny zespołu</string>
    <string id="AutoMountTeamSites_help">To ustawienie pozwala określić, że biblioteki witryny zespołu programu SharePoint mają być automatycznie synchronizowane przy następnym logowaniu użytkowników do klienta synchronizacji usługi OneDrive (OneDrive.exe). Aby użyć tego ustawienia, należy włączyć funkcję Pliki na żądanie usługi OneDrive. To ustawienie dotyczy tylko użytkowników na komputerach z systemem Windows 10 Fall Creators Update lub nowszym. Ta funkcja nie jest włączona dla lokalnych witryn programu SharePoint.

Jeśli włączysz to ustawienie, przy następnym logowaniu użytkownika klient synchronizacji usługi OneDrive automatycznie pobierze zawartość bibliotek określoną jako pliki tylko w trybie online. Użytkownik nie będzie mógł zatrzymać synchronizacji bibliotek. 

Jeśli wyłączysz to ustawienie, określone biblioteki witryny zespołu nie będą automatycznie synchronizowane dla nowych użytkowników. Istniejący użytkownicy mogą zatrzymać synchronizację bibliotek, ale nie będzie ona zatrzymywana automatycznie.</string>
    <!-- Insert multi-tenant settings here -->
    <!-- See http://go.microsoft.com/fwlink/p/?LinkId=797547 for configuration instructions -->

    </stringTable>
    <presentationTable>
      <presentation id="AutomaticUploadBandwidthPercentage_Pres">
        <text>Wybierz maksymalny procent przepustowości do zajęcia podczas przekazywania plików.</text>
        <text>Prawidłowe wartości to liczby z przedziału 10–99.</text>
        <decimalTextBox refId="BandwidthSpinBox" defaultValue="70" spinStep="1">Przepustowość:</decimalTextBox>
      </presentation>

      <presentation id="UploadBandwidthLimit_Pres">
        <text>Wybierz maksymalną ilość przepustowości do zajęcia podczas przekazywania plików.</text>
        <text>Prawidłowe wartości to liczby od 1 do 100000.</text>
        <decimalTextBox refId="UploadRateValue" defaultValue="125">Przepustowość:</decimalTextBox>
      </presentation>

      <presentation id="DownloadBandwidthLimit_Pres">
        <text>Wybierz maksymalną ilość przepustowości do zajęcia podczas pobierania plików.</text>
        <text>Prawidłowe wartości to liczby od 1 do 100000.</text>
        <decimalTextBox refId="DownloadRateValue" defaultValue="125">Przepustowość:</decimalTextBox>
      </presentation>

       <presentation id="DiskSpaceCheckThresholdMB_Pres">
        <text>Określ identyfikator GUID dzierżawy i maksymalny rozmiar usługi OneDrive dla Firm użytkownika przed wyświetleniem mu monitu o wybranie folderów do zsynchronizowania. </text>
        <text>W polu nazwy wpisz identyfikator GUID dzierżawy. W polu wartości wprowadź rozmiar.</text>
        <text>Prawidłowe wartości to liczby od 0 do 4294967295 MB (włącznie).</text>
        <listBox refId="DiskSpaceCheckThresholdMBList">Ścieżki dzierżawy: </listBox>
      </presentation>

      <presentation id="DefaultRootDir_Pres">
       <text>Określ ścieżkę domyślną i identyfikator GUID dzierżawy. </text>
        <text>W polu nazwy wpisz identyfikator GUID dzierżawy. W polu wartości wprowadź ścieżkę.</text>
        <listBox refId="DefaultRootDirList">Ścieżki dzierżawy: </listBox>
      </presentation>
      
      <presentation id="DisableCustomRoot_Pres">
        <text>Określ identyfikator GUID dzierżawy i wartość ustawienia. Wartość 1 włącza to ustawienie, wartość 0 wyłącza to ustawienie. </text>
        <text>W polu nazwy wpisz identyfikator GUID dzierżawy. W polu wartości wprowadź 1 lub 0.</text>
        <listBox refId="DisableCustomRootList">Ścieżki dzierżawy: </listBox>
      </presentation>
      
      <presentation id="AllowTenantList_Pres">
        <text>Określ identyfikator dzierżawy</text>
        <text>W polu wartości wpisz identyfikator dzierżawy, który chcesz dodać do tej listy</text>
        <text> </text>
        <listBox refId="AllowTenantListBox">Identyfikator GUID dzierżawy: </listBox>
      </presentation>
      
      <presentation id="BlockTenantList_Pres">
        <text>Określ identyfikator dzierżawy</text>
        <text>W polu wartości wpisz identyfikator dzierżawy, który chcesz dodać do tej listy</text>
        <text> </text>
        <listBox refId="BlockTenantListBox">Identyfikator GUID dzierżawy: </listBox>
       </presentation>

      <presentation id="SharePointOnPremFrontDoorUrl_Pres">
        <text>Podaj adres URL lokalnego serwera programu SharePoint, który hostuje usługę OneDrive dla Firm użytkownika, i nazwę folderu dzierżawy.</text>
        <textBox refId="SharePointOnPremFrontDoorUrlBox">
          <label>Adres URL lokalnego serwera programu SharePoint:</label>
        </textBox>
        <textBox refId="SharePointOnPremTenantNameBox">
          <label>Nazwa folderu dzierżawy:</label>
        </textBox>
      </presentation>

      <presentation id="SharePointOnPremPrioritization_Pres">
        <dropdownList refId="SharePointOnPremPrioritization_Dropdown" noSort="true" defaultItem="0">Ustawienie priorytetyzacji w programie SharePoint dla klientów modelu hybrydowego</dropdownList>
      </presentation>
      
      <presentation id="BlockKnownFolderMove_Pres">
        <dropdownList refId="BlockKnownFolderMove_Dropdown" noSort="true" defaultItem="0">Jeśli znane foldery zostały już przeniesione do usługi OneDrive:</dropdownList>
      </presentation>
      
      <presentation id="KFMOptInWithWizard_Pres">
        <textBox refId="KFMOptInWithWizard_TextBox">
          <label>Identyfikator dzierżawy:</label>
        </textBox>
      </presentation> 
      
      <presentation id="KFMOptInNoWizard_Pres">
        <textBox refId="KFMOptInNoWizard_TextBox">
          <label>Identyfikator dzierżawy:</label>
        </textBox>
        <dropdownList refId="KFMOptInNoWizard_Dropdown" noSort="true" defaultItem="0">Pokaż użytkownikom powiadomienie po przekierowaniu folderów:</dropdownList>
      </presentation>
      <presentation id="AutoMountTeamSites_Pres">
        <text>Aby określić bibliotekę do synchronizacji:

Otwórz przeglądarkę sieci Web, zaloguj się do usługi Office 365 jako administrator globalny lub administrator programu SharePoint dla organizacji i przejdź do biblioteki. 

Kliknij przycisk „Synchronizuj” w bibliotece, która ma być synchronizowana automatycznie, a następnie kliknij pozycję „Kopiuj identyfikator biblioteki”.

Kliknij pozycję „Pokaż”, aby wprowadzić identyfikator biblioteki oraz identyfikator w polu nazwy.
        </text>
        <listBox refId="AutoMountTeamSitesListBox">Biblioteki</listBox>
      </presentation>
    </presentationTable>
  </resources>
</policyDefinitionResources>
