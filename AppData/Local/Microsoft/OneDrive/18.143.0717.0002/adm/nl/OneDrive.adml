<?xml version="1.0" encoding="utf-8"?>
<!-- (c) 2016 Microsoft Corporation -->
<policyDefinitionResources xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" revision="1.0" schemaVersion="1.0" xmlns="http://www.microsoft.com/GroupPolicy/PolicyDefinitions">
  <displayName>Groepsbeleidsinstellingen voor OneDrive</displayName>
  <description>Verschillende groepsbeleidsinstellingen voor de OneDrive-synchronisatieclient, met name voor het configureren van instellingen die te maken hebben met bedrijfsfunctionaliteit in de client.</description>
  <resources>
    <stringTable>
      <!-- general -->
      <string id="OneDriveNGSCSettingCategory">OneDrive</string>
      
      <!-- block syncing personal OneDrive -->
      <string id="DisablePersonalSync">Voorkomen dat gebruikers persoonlijke OneDrive-accounts synchroniseren</string>
      <string id="DisablePersonalSync_help">Met deze instelling kunt u verhinderen dat gebruikers bestanden synchroniseren vanuit een consumentenaccount in OneDrive (gebaseerd op Microsoft-account). Standaard is ingesteld dat gebruikers persoonlijke OneDrive-accounts kunnen synchroniseren.

Als u deze instelling inschakelt, wordt voorkomen dat gebruikers een synchronisatierelatie tot stand brengen voor hun persoonlijke OneDrive-account. Als ze eerder een persoonlijk OneDrive-account hebben gesynchroniseerd, krijgen ze een foutmelding te zien wanneer ze de synchronisatieclient starten, maar hun bestanden blijven behouden op de schijf.

Als u deze instelling uitschakelt, kunnen gebruikers hun persoonlijke OneDrive-account synchroniseren.</string>

      <!-- turn on enterprise tier cadence for app updates -->
      <string id="EnableEnterpriseUpdate">Bijwerken van OneDrive.exe uitstellen tot de tweede releasegolf</string>
      <string id="EnableEnterpriseUpdate_help">Updates voor OneDrive.exe worden in twee golven uitgebracht. De eerste golf wordt gestart zodra er een update beschikbaar is. Meestal duurt het een of twee weken om de implementatie te voltooien. De tweede golf wordt gestart nadat de eerste is voltooid.

Deze instelling zorgt ervoor dat OneDrive-synchronisatieclients pas worden bijgewerkt bij de tweede golf. Hierdoor hebt u extra tijd om u voor te bereiden op toekomstige updates. 

Updates worden standaard geïnstalleerd zodra ze beschikbaar zijn tijdens de eerste golf.

Als u deze instelling inschakelt, worden de OneDrive-synchronisatieclients in uw domein bijgewerkt tijdens de tweede golf, enkele weken nadat de updates in brede kring zijn vrijgegeven aan alle Office 365-klanten.

Als u deze instelling uitschakelt, worden OneDrive-synchronisatieclients bijgewerkt zodra er updates beschikbaar zijn tijdens de eerste golf.</string>

      <!-- set default location of the OneDrive folder -->
      <string id="DefaultRootDir">Standaardlocatie voor de OneDrive-map instellen</string>
      <string id="DefaultRootDir_help">Met deze instelling kunt u een specifiek pad instellen als standaardlocatie voor de OneDrive-map wanneer gebruikers de wizard Welkom bij OneDrive doorlopen tijdens het configureren van bestandssynchronisatie. Het pad bevindt zich standaard onder %userprofile%.

Als u deze instelling inschakelt, wordt de standaardlocatie voor de lokale OneDrive - {tenant name}-map het pad dat u opgeeft in het bestand OneDrive.admx.

Als u deze instelling uitschakelt, is %userprofile% de standaardlocatie van de lokale OneDrive - {tenant name}-map.</string>

      <!-- disable changing the default location of the OneDrive folder -->
      <string id="DisableCustomRoot">Voorkomen dat gebruikers de locatie van hun OneDrive-map wijzigen</string>
      <string id="DisableCustomRoot_help">Met deze instelling kunt u voorkomen dat gebruikers de locatie van hun OneDrive-synchronisatiemap wijzigen.

Als u deze instelling inschakelt, kunnen gebruikers de locatie van hun OneDrive - {tenant name}-map niet wijzigen tijdens de wizard Welkom bij OneDrive. Gebruikers worden gedwongen om de standaardlocatie te gebruiken, of dit zorgt ervoor (indien u de instelling Standaardlocatie voor de OneDrive-map instellen hebt ingesteld) dat de lokale OneDrive-map van alle gebruikers zich op de locatie bevindt die u hebt opgegeven.

Als u deze instelling uitschakelt, kunnen gebruikers de locatie van hun synchronisatie wijzigen tijdens de wizard Welkom bij OneDrive.</string>

      <!-- Enable Office Integration for coauthoring and in-app sharing -->
      <string id="EnableAllOcsiClients">Cocreatie en in-app delen voor Office-bestanden</string>
      <string id="EnableAllOcsiClients_help">Met deze instelling is livecocreatie en in-app delen mogelijk voor Office-bestanden die lokaal zijn geopend op de computer. Cocreatie en in-app delen voor Office-bestanden is standaard toegestaan. (Cocreatie is beschikbaar in Office 2013 en Office 2016.)

Als u deze instelling inschakelt, is cocreatie en in-app delen voor Office ingeschakeld, maar gebruikers kunnen deze instelling uitschakelen op het tabblad Office in de synchronisatieclient, indien ze willen.

Als u deze instelling uitschakelt, is cocreatie en in-app delen voor Office-bestanden uitgeschakeld en is het tabblad Office verborgen in de synchronisatieclient. Als u deze instelling uitschakelt, is de instelling Gebruikers kunnen kiezen hoe Office-bestanden moeten worden verwerkt bij een conflict ook uitgeschakeld. Indien zich een conflict voordoet, wordt het bestand gesplitst.</string>


      <!-- Enable hold the file for handling Office conflicts -->
      <string id="EnableHoldTheFile">Gebruikers kunnen kiezen hoe conflicterende Office-bestanden moeten worden verwerkt</string>
      <string id="EnableHoldTheFile_help">Deze instelling bepaalt wat er gebeurt wanneer tijdens de synchronisatie een conflict optreedt tussen Office 2016-bestandsversies. Standaard kunnen gebruikers zelf beslissen of ze wijzigingen willen samenvoegen of beide exemplaren willen behouden. Gebruikers kunnen de synchronisatieclient ook zo configureren dat het bestand altijd wordt gesplitst en beide exemplaren behouden blijven. (Deze optie is alleen beschikbaar voor Office 2016. In eerdere versies van Office wordt het bestand altijd gesplitst en blijven beide exemplaren behouden.)

Als u deze instelling inschakelt, kunnen gebruikers zelf beslissen of ze wijzigingen willen samenvoegen of beide exemplaren willen behouden. Gebruikers kunnen de client ook zo synchroniseren dat het bestand altijd wordt gesplitst en beide exemplaren behouden blijven.

Als u deze instelling uitschakelt, wordt het bestand altijd gesplitst en blijven beide exemplaren behouden wanneer er een synchronisatieconflict optreedt. De configuratie-instelling in de synchronisatie-client is uitgeschakeld.</string>

      <!-- Enable Automatic Upload Bandwidth Limiting -->
      <string id="AutomaticUploadBandwidthPercentage">Maximale percentage bandbreedte voor uploaden instellen waarvan OneDrive.exe gebruikmaakt</string>
      <string id="AutomaticUploadBandwidthPercentage_help">Met deze instelling kunt u het maximale percentage beschikbare bandbreedte op de computer configureren waarvan OneDrive-synchronisatie gebruikmaakt om te uploaden. (OneDrive gebruikt deze bandbreedte alleen tijdens het synchroniseren van bestanden.) De bandbreedte die beschikbaar is voor een computer, verandert voortdurend. Door een percentage te gebruiken, kan de synchronisatie reageren op zowel toegenomen als afgenomen beschikbaarheid van bandbreedte tijdens synchronisatie op de achtergrond. Hoe lager het percentage bandbreedte waarvan OneDrive-synchronisatie gebruik mag maken, hoe langzamer bestanden worden gesynchroniseerd op de computer. U wordt aangeraden te kiezen voor een percentage van 50% of hoger. Synchronisatie maakt uploadbeperkingen mogelijk door de synchronisatie-engine periodiek gedurende één minuut op volledige snelheid uit te laten voeren en vervolgens het uploaden te vertragen naar het percentage dat via deze instelling is bepaald. Hierdoor zijn twee hoofdscenario's mogelijk. Ten eerste, heel kleine bestanden worden heel snel geüpload omdat deze passen in het interval waarin de maximale uploadsnelheid wordt gemeten. Ten tweede, voor uploads die lang duren wordt de uploadsnelheid nog steeds geoptimaliseerd volgens het percentage dat via deze instelling is bepaald.

Als u deze instelling inschakelt, wordt het maximale percentage bandbreedte dat u opgeeft, gebruikt op computers waarop dit beleid van toepassing is.

Als u deze instelling uitschakelt, kunnen gebruikers zelf bepalen hoeveel bandbreedte voor uploaden ze op de computer gebruiken.

Als u deze instelling in- of uitschakelt, moet u de instelling niet terugzetten naar Niet geconfigureerd. Als u dit doet, wordt de configuratie niet gewijzigd en blijft de laatst geconfigureerde instelling van kracht.</string>

      <!-- Enable Upload Bandwidth Limiting -->
      <string id="UploadBandwidthLimit">Maximale bandbreedte voor uploaden instellen waarvan OneDrive.exe gebruikmaakt</string>
      <string id="UploadBandwidthLimit_help">Met deze instelling kunt u de maximale bandbreedte configureren die beschikbaar is op de computer die OneDrive-synchronisatie gebruikt om te uploaden. (OneDrive gebruikt deze bandbreedte alleen tijdens het synchroniseren van bestanden.) Deze bandbreedtelimiet is een vaste snelheid in kilobytes per seconde. Hoe lager de bandbreedte die beschikbaar is voor OneDrive-synchronisatie, hoe langzamer bestanden worden gesynchroniseerd op de computer. De minimale snelheid die u kunt instellen, is 1 kB/s en de maximale snelheid is 100.000 kB/s. Elke invoer die lager is dan 50 kB/s, wordt ingesteld op 50 kB/s, zelfs als op de gebruikersinterface de opgegeven snelheid wordt weergegeven.

Als u deze instelling inschakelt, wordt de door u opgegeven maximale bandbreedte voor uploaden gebruikt op computers waarop dit beleid van toepassing is.

Als u deze instelling uitschakelt, kunnen gebruikers zelf bepalen hoeveel bandbreedte voor uploaden kan worden gebruikt op de computer.</string>

      <!-- Enable Download Bandwidth Limiting -->
      <string id="DownloadBandwidthLimit">Maximale bandbreedte voor downloaden instellen die wordt gebruikt voor OneDrive.exe</string>
      <string id="DownloadBandwidthLimit_help">Met deze instelling kunt u de maximale bandbreedte configureren die beschikbaar is op de computer die OneDrive-synchronisatie gebruikt om te downloaden. (OneDrive gebruikt deze bandbreedte alleen tijdens het synchroniseren van bestanden.) Deze bandbreedtelimiet is een vaste snelheid in kilobytes per seconde. Hoe lager de bandbreedte die beschikbaar is voor OneDrive-synchronisatie, hoe langzamer bestanden worden gesynchroniseerd op de computer. De minimale snelheid die u kunt instellen, is 1 kB/s en de maximale snelheid is 100.000 kB/s. Elke invoer die lager is dan 50 kB/s, wordt ingesteld op 50 kB/s, zelfs als op de gebruikersinterface de opgegeven snelheid wordt weergegeven.

Als u deze instelling inschakelt, wordt de door u opgegeven maximale bandbreedte voor downloaden gebruikt op computers waarop dit beleid van toepassing is.

Als u deze instelling uitschakelt, kunnen gebruikers zelf bepalen hoeveel bandbreedte voor downloaden kan worden gebruikt op de computer.</string>
      <!-- turn off remote access/fetch on the computer (32-bit) -->
      <string id="RemoteAccessGPOEnabled">Voorkomen dat gebruikers toegang krijgen tot bestanden op de computer met behulp van de functie voor het ophalen van externe bestanden</string>
      <string id="RemoteAccessGPOEnabled_help">Met deze instelling kunt u voorkomen dat gebruikers de ophaalfunctie gebruiken wanneer ze zijn aangemeld bij OneDrive.exe via hun Microsoft-account. Met de ophaalfunctie kunnen gebruikers naar OneDrive.com gaan, een Windows-computer selecteren die op dat moment online is en waarop de OneDrive-synchronisatieclient wordt uitgevoerd, en al hun persoonlijke bestanden openen op deze computer. Standaard kunnen gebruikers de ophaalfunctie gebruiken.

Als u deze instelling inschakelt, kunnen gebruikers de ophaalfunctie niet meer gebruiken.

Als u deze instelling uitschakelt, kunnen gebruikers de ophaalfunctie gebruiken.

Deze instelling is bedoeld voor computers met de 32-bits of 64-bits versie van Windows.</string>

      <!-- prevent OneDrive sync client (OneDrive.exe) from generating network traffic (checking for updates, etc.) until the user signs in to OneDrive -->
      <string id="PreventNetworkTrafficPreUserSignIn">Voorkomen dat OneDrive netwerkverkeer genereert totdat de gebruiker zich aanmeldt bij OneDrive</string>
      <string id="PreventNetworkTrafficPreUserSignIn_help">Schakel deze instelling in om te voorkomen dat de OneDrive-synchronisatieclient (OneDrive.exe) netwerkverkeer genereert (controleren op updates, enzovoort) totdat de gebruiker zich aanmeldt bij OneDrive of begint met het synchroniseren van bestanden naar de lokale computer.

Als u deze instelling inschakelt, moeten gebruikers zich aanmelden bij de OneDrive-synchronisatieclient op de lokale computer of ervoor kiezen om OneDrive- of SharePoint-bestanden te synchroniseren op de computer, om de synchronisatieclient automatisch te laten starten.

Als deze instelling niet is ingeschakeld, wordt de OneDrive-synchronisatieclient automatisch gestart wanneer gebruikers zich aanmelden bij Windows.

Als u deze instelling in- of uitschakelt, zet u de instelling niet terug naar Niet geconfigureerd. Hierdoor wordt de configuratie niet gewijzigd en blijft de laatste instelling van kracht.</string>

      <!-- Silent Account Config -->
      <string id="SilentAccountConfig">OneDrive op de achtergrond configureren met behulp van het primaire Windows-account</string>
      <string id="SilentAccountConfig_help">Met deze instelling kunt u OneDrive op de achtergrond configureren met behulp van het primaire Windows-account. 

Als u deze instelling inschakelt, probeert OneDrive aan te melden bij OneDrive voor Bedrijven met behulp van deze referenties. Vóór het synchroniseren wordt de beschikbare schijfruimte gecontroleerd. Als er veel ruimte is, wordt de gebruiker gevraagd om mappen kiezen. De drempel voor het stellen van deze vraag kan worden geconfigureerd via DiskSpaceCheckThresholdMB. OneDrive probeert aan te melden bij elk account op de computer. Zodra aanmelden is gelukt, wordt op het betreffende account geen poging meer ondernomen tot configureren op de achtergrond.

Als u deze instelling inschakelt, moet ADAL zijn ingeschakeld. Als dit niet het geval is, mislukt de accountconfiguratie.

Als u deze instelling inschakelt en de gebruiker de verouderde OneDrive voor Bedrijven-synchronisatieclient gebruikt, probeert de nieuwe client om de synchronisatie over te nemen van de verouderde client. Als dit lukt, blijven de synchronisatie-instellingen van de gebruiker van de verouderde client behouden.

Als u deze instelling uitschakelt, probeert OneDrive niet om gebruikers automatisch aan te melden.

Andere instellingen die handig zijn met SilentAccountConfig, zijn bijvoorbeeld DiskSpaceCheckThresholdMB en DefaultRootDir.
      </string>

      <!-- DiskSpaceCheckThresholdMB -->
      <string id="DiskSpaceCheckThresholdMB">De maximale grootte van het OneDrive voor Bedrijven-account van een gebruiker voordat deze wordt gevraagd om te kiezen welke mappen moeten worden gedownload</string>
      <string id="DiskSpaceCheckThresholdMB_help">Deze instelling wordt gebruikt in combinatie met SilentAccountConfig. Iedere gebruiker met een OneDrive voor Bedrijven-account dat groter is dan de opgegeven drempel (in MB), wordt gevraagd te kiezen welke mappen ze willen synchroniseren voordat bestanden worden gedownload.
      </string>

      <!-- Settings below control behavior of Files-On-Demand (Cloud Files) -->
      <string id="FilesOnDemandEnabled">OneDrive Bestanden op aanvraag inschakelen</string>
      <string id="FilesOnDemandEnabled_help">Met deze instelling kunt u expliciet beheren of OneDrive Bestanden op aanvraag is ingeschakeld voor de tenant.

Als u deze instelling inschakelt, wordt OneDrive Bestanden op aanvraag standaard ingeschakeld voor alle gebruikers op wie het beleid van toepassing is.

Als u deze instelling uitschakelt, wordt OneDrive Bestanden op aanvraag expliciet uitgeschakeld. De instelling kan niet worden ingeschakeld door gebruikers.

Als u deze instelling niet configureert, kunnen gebruikers OneDrive Bestanden op aanvraag in- of uitschakelen.
      </string>
      
      <string id="DehydrateSyncedTeamSites">Bestaande teamsites migreren met OneDrive Bestanden op aanvraag</string>
      <string id="DehydrateSyncedTeamSites_help">Dit beleid is van toepassing als OneDrive Bestanden op aanvraag is ingeschakeld.

Op basis van dit beleid kunt u eerder gedownloade teamsite-inhoud migreren zodat deze het kenmerk Alleen-online krijgt.

Als u dit beleid inschakelt, worden teamsites die zijn gesynchroniseerd voordat OneDrive Bestanden op aanvraag was ingeschakeld, standaard overgeschakeld naar Alleen-online.
      
Dit is ideaal voor gevallen waarbij u bandbreedte wilt besparen en veel pc's dezelfde teamsite synchroniseren.
      </string>      
      
      <string id="AllowTenantList">Synchroniseren van OneDrive-accounts alleen toestaan voor specifieke organisaties</string>
      <string id="AllowTenantList_help">Met deze instelling kunt u voorkomen dat gebruikers eenvoudig bestanden kunnen uploaden naar andere organisaties door een lijst met toegestane tenant-id's op te geven. 

Als u deze instelling inschakelt, zien gebruikers een foutmelding als ze proberen een account toe te voegen van een organisatie die niet is toegestaan. Als een gebruiker het account al heeft toegevoegd, stopt het synchroniseren van de bestanden.

Als u deze instelling uitschakelt of niet configureert, kunnen gebruikers accounts toevoegen van elke organisatie. 

Als u in plaats hiervan specifieke organisaties wilt blokkeren, gebruikt u de instelling: Synchroniseren van OneDrive-accounts blokkeren voor specifieke organisaties.

Deze instelling overschrijft het beleid Synchroniseren van OneDrive-accounts blokkeren voor specifieke organisaties. Schakel beide beleidsregels niet tegelijkertijd in.
      </string>
      
      <string id="BlockTenantList">Synchroniseren van OneDrive-accounts blokkeren voor specifieke organisaties</string>
      <string id="BlockTenantList_help">
Met deze instelling kunt u voorkomen dat gebruikers eenvoudig bestanden kunnen uploaden naar andere organisaties door een lijst met geblokkeerde tenant-id's op te geven. 

Als u deze instelling inschakelt, zien gebruikers een foutmelding als ze proberen een account toe te voegen van een organisatie die is geblokkeerd. Als een gebruiker het account al heeft toegevoegd, stopt het synchroniseren van de bestanden.

Als u deze instelling uitschakelt of niet configureert, kunnen gebruikers accounts toevoegen van elke organisatie. 

Als u een lijst met toegestane organisatie wilt opgeven, gebruikt u de instelling: Synchroniseren van OneDrive-accounts alleen toestaan voor specifieke organisaties.

Deze instelling werkt NIET als dit beleid is ingeschakeld: Synchroniseren van OneDrive-accounts alleen toestaan voor specifieke organisaties. Schakel beide beleidsregels niet tegelijkertijd in.
      </string>

    <!-- SharePoint On-Prem front door URL -->
    <string id="SharePointOnPremFrontDoorUrl">URL voor on-premises SharePoint-server en naam van tenantmap</string>
    <string id="SharePointOnPremFrontDoorUrl_help">Met deze beleidsinstelling kunt u een URL voor een on-premises SharePoint-server instellen. De URL is vereist wanneer gebruikers hun OneDrive voor Bedrijven-accounts willen synchroniseren die on-premises zijn gehost. De tenantnaam is vereist om een correct naamgevingsschema op te geven voor de hoofdmap.

Als u deze instelling inschakelt en zowel de on-premises SharePoint-URL als de naam van de tenantmap opgeeft, kunnen gebruikers hun on-premises gehoste OneDrive voor Bedrijven-accounts synchroniseren.r

Als u deze instelling uitschakelt of de ingang-URL of naam van de tenantmap niet configureert, kunnen gebruikers hun on-premises gehoste OneDrive voor Bedrijven-accounts niet synchroniseren.
    </string>

    <!-- SharePoint on-Prem prioritization settings -->
    <string id="SharePointOnPremPrioritization">SharePoint-prioriteitsinstelling voor klanten van de hybride versie die SPO (SharePoint Online) en een on-premises SharePoint-server gebruiken</string>
    <string id="SharePointOnPremPrioritization_help">Met behulp van deze beleidsinstelling kunt u configureren op welke locatie via de OneDrive-synchronisatieclient moet worden gezocht naar de persoonlijke OneDrive voor Bedrijven-site van een gebruiker (Mijn site) in een hybride omgeving, nadat de gebruiker zich heeft aangemeld.

Als u deze instelling wilt gebruiken, moet u het groepsbeleid voor URL’s voor on-premises SharePoint-servers configureren. Deze instelling is alleen van invloed op synchronisatiefuncties in OneDrive voor Bedrijven. Gebruikers kunnen nog steeds teamsites in on-premises SPO of SharePoint synchroniseren, ongeacht deze instelling.

Als u deze instelling inschakelt, kunt u een van deze twee opties selecteren:

PrioritizeSPO: met de synchronisatieclient wordt eerst in SPO gezocht naar de persoonlijke OneDrive voor Bedrijven-site van een gebruiker, voordat op de on-premises SharePoint-server wordt gezocht. Als voor de aangemelde gebruiker SPO al is geconfigureerd op de synchronisatieclient, wordt een poging gedaan om een OneDrive voor Bedrijven-instantie in on-premises SharePoint te configureren voor deze gebruiker.

PrioritizeSharePointOnPrem: met de synchronisatieclient wordt eerst op de on-premises SharePoint-server gezocht naar de persoonlijke OneDrive voor Bedrijven-site van een gebruiker, voordat in SPO wordt gezocht. Als voor de aangemelde gebruiker al een on-premises SharePoint-server is geconfigureerd op de synchronisatieclient, wordt een poging gedaan om een OneDrive voor Bedrijven-instantie in SPO te configureren voor deze gebruiker.

Als u deze instelling uitschakelt, is het gedrag zoals in de optie PrioritizeSPO.
    </string>
    <string id="PrioritizeSPO">Prioriteit geven aan het synchroniseren van een persoonlijke OneDrive voor Bedrijven-site van een gebruiker met SPO</string>
    <string id="PrioritizeSharePointOnPrem">Prioriteit geven aan het synchroniseren van een persoonlijke OneDrive voor Bedrijven-site van een gebruiker met een on-premises SharePoint-server</string>
    
    <!-- Disable tutorial in the FRE -->
    <string id="DisableFRETutorial">Voorkomen dat gebruikers de zelfstudie zien na aanmelden in OneDrive</string>
    <string id="DisableFRETutorial_help">Met deze instelling voorkomt u dat gebruikers de webbrowserzelfstudie kunnen starten aan het eind van een OneDrive First Run-sessie

Als u deze instelling inschakelt, is de zelfstudie niet zichtbaar voor gebruikers die zich hebben aangemeld.

Als u de instelling uitschakelt, krijgen gebruikers te maken met het oorspronkelijke gedrag. Het uitschakelen van deze instelling heeft hetzelfde effect als wanneer u deze instelling niet configureert</string>
      <!-- Block KFM -->
      <string id="BlockKnownFolderMove">Voorkomen dat gebruikers hun bekende mappen in Windows naar OneDrive verplaatsen</string>
      <string id="BlockKnownFolderMove_help">Met deze instelling wordt voorkomen dat gebruikers hun mappen Documenten, Afbeeldingen en Bureaublad verplaatsen naar een OneDrive voor Bedrijven-account.
Opmerking: het verplaatsen van bekende mappen naar persoonlijke OneDrive-accounts is al geblokkeerd op pc's die lid zijn van een domein.

Als u deze instelling inschakelt, wordt het venster Beveiliging instellen voor belangrijke mappen niet weergegeven voor gebruikers, en is de optie Beveiliging starten uitgeschakeld. Als gebruikers hun bekende mappen al hebben verplaatst, blijven de bestanden in deze mappen beschikbaar in OneDrive. Dit beleid wordt niet doorgevoerd als u Gebruikers vragen bekende mappen in Windows te verplaatsen naar OneDrive of Bekende mappen in Windows op de achtergrond omleiden naar OneDrive hebt ingeschakeld.

Als u deze instelling uitschakelt of niet configureert, kunnen gebruikers ervoor kiezen om hun bekende mappen te verplaatsen. 
    </string>
    <!-- KFMOptInWithWizard -->
    <string id="KFMOptInWithWizard">Gebruikers vragen bekende mappen in Windows te verplaatsen naar OneDrive</string>
    <string id="KFMOptInWithWizard_help">Met deze instelling wordt het venster Beveiliging van belangrijke mappen instellen weergegeven waarin gebruikers wordt gevraagd om hun mappen Documenten, Afbeeldingen en Bureaublad te verplaatsen naar OneDrive.

Als u deze instelling inschakelt en uw tenant-id opgeeft, zien gebruikers die hun OneDrive synchroniseren, het venster Beveiliging van belangrijke mappen instellen, wanneer ze zijn aangemeld. Als ze het venster sluiten, verschijnt er een herinneringsmelding in het activiteitencentrum totdat ze alle drie bekende mappen hebben verplaatst. Als gebruikers al mappen voor een ander account hebben verplaatst, wordt hun gevraagd om de mappen om te leiden naar het nieuwe account (waarbij bestaande mappen niet worden verplaatst).

Als u deze instelling uitschakelt of niet configureert, wordt het venster Beveiliging van belangrijke mappen instellen niet automatisch weergegeven voor gebruikers. 
    </string>
    <!-- KFMOptInNoWizard -->
    <string id="KFMOptInNoWizard">Bekende mappen in Windows op de achtergrond omleiden naar OneDrive</string>
    <string id="KFMOptInNoWizard_help">Met deze instelling kunt u de mappen Documenten, Afbeeldingen en Bureaublad van gebruikers omleiden naar OneDrive zonder tussenkomst van de gebruiker. Dit beleid werkt wanneer alle bekende mappen leeg zijn, en voor mappen die zijn omgeleid naar een ander OneDrive-account. We raden u aan dit beleid te gebruiken samen met Gebruikers vragen bekende mappen in Windows te verplaatsen naar OneDrive.
 
Als u dit beleid inschakelt, wordt in toekomstige releases niet meer gecontroleerd op lege bekende mappen. In plaats hiervan worden bekende mappen omgeleid en wordt alle inhoud in deze mappen verwijderd.
 
Als u deze instelling inschakelt en uw tenant-id opgeeft, kunt u bepalen of er een melding wordt weergegeven voor gebruikers nadat hun mappen zijn omgeleid.

Als u deze instelling uitschakelt of niet configureert, worden de bekende mappen van gebruikers niet op de achtergrond omgeleid naar OneDrive. 
    </string>
    <string id="KFMOptInNoWizardToast">Ja</string>
    <string id="KFMOptInNoWizardNoToast">Nee</string>
     <!-- Block KFM Opt Out -->
      <string id="KFMBlockOptOut">Voorkomen dat gebruikers hun bekende mappen in Windows omleiden naar de pc</string>
      <string id="KFMBlockOptOut_help">Met deze instelling worden gebruikers gedwongen om hun mappen Documenten, Afbeeldingen en Bureaublad altijd om te leiden naar OneDrive.
      
Als u deze instelling inschakelt, wordt de knop Beveiliging stoppen uitgeschakeld in het venster Beveiliging van belangrijke mappen instellen. Gebruikers ontvangen vervolgens een foutmelding wanneer ze het synchroniseren van een bekende map willen stoppen.

Als u deze instelling uitschakelt of niet configureert, kunnen gebruikers ervoor kiezen om hun bekende mappen om te leiden naar de pc. 
    </string>
    <string id="AutoMountTeamSites">Teamsitebibliotheken configureren voor automatische synchronisatie</string>
    <string id="AutoMountTeamSites_help">Met deze instelling kunt u SharePoint-teamsitebibliotheken automatisch laten synchroniseren wanneer de gebruiker zich de volgende keer aanmeldt bij de OneDrive-synchronisatieclient (OneDrive.exe). Als u de instelling wilt gebruiken, moet u OneDrive Bestanden op aanvraag inschakelen. De instelling is alleen van toepassing voor gebruikers met computers waarop Windows 10 Fall Creators Update of later wordt uitgevoerd. Deze functie is niet ingeschakeld voor on-premises SharePoint-sites.
 
Als u deze instelling inschakelt, wordt de volgende keer dat de gebruiker zich aanmeldt met de OneDrive-synchronisatieclient automatisch de inhoud gedownload uit de bibliotheken die u hebt gemarkeerd als Alleen-online. De gebruiker kan het synchroniseren van de bibliotheken niet stoppen. 
 
Als u deze instelling uitschakelt, worden de opgegeven teamsitebibliotheken niet automatisch gesynchroniseerd voor nieuwe gebruikers. Bestaande gebruikers kunnen ervoor kiezen om het synchroniseren van de bibliotheken te stoppen, maar het synchroniseren van de bibliotheken wordt niet automatisch gestopt.</string>
    <!-- Insert multi-tenant settings here -->
    <!-- See http://go.microsoft.com/fwlink/p/?LinkId=797547 for configuration instructions -->

    </stringTable>
    <presentationTable>
      <presentation id="AutomaticUploadBandwidthPercentage_Pres">
        <text>Selecteer het maximale percentage bandbreedte dat kan worden gebruikt voor het uploaden van bestanden.</text>
        <text>Geldige waarden liggen tussen 10 en 99.</text>
        <decimalTextBox refId="BandwidthSpinBox" defaultValue="70" spinStep="1">Bandbreedte:</decimalTextBox>
      </presentation>

      <presentation id="UploadBandwidthLimit_Pres">
        <text>Selecteer de maximale hoeveelheid bandbreedte die kan worden gebruikt voor het uploaden van bestanden.</text>
        <text>Geldige waarden liggen tussen 1 en 100.000.</text>
        <decimalTextBox refId="UploadRateValue" defaultValue="125">Bandbreedte:</decimalTextBox>
      </presentation>

      <presentation id="DownloadBandwidthLimit_Pres">
        <text>Selecteer de maximale hoeveelheid bandbreedte die kan worden gebruikt voor het downloaden van bestanden.</text>
        <text>Geldige waarden liggen tussen 1 en 100.000.</text>
        <decimalTextBox refId="DownloadRateValue" defaultValue="125">Bandbreedte:</decimalTextBox>
      </presentation>

       <presentation id="DiskSpaceCheckThresholdMB_Pres">
        <text>Geef de tenant-GUID en de maximale grootte op van het OneDrive voor Bedrijven-account van een gebruiker vóórdat de gebruiker wordt gevraagd de mappen te kiezen die moeten worden gesynchroniseerd. </text>
        <text>Typ in het naamveld de tenant-GUID. Voer in het waardeveld de grootte in.</text>
        <text>Geldige waarden liggen tussen 0 en 4.294.967.295 MB (inclusief).</text>
        <listBox refId="DiskSpaceCheckThresholdMBList">Tenantpaden: </listBox>
      </presentation>

      <presentation id="DefaultRootDir_Pres">
       <text>Geef de tenant-GUID en het standaardpad op. </text>
        <text>Typ in het naamveld de tenant-GUID. Voer in het waardeveld het pad in.</text>
        <listBox refId="DefaultRootDirList">Tenantpaden: </listBox>
      </presentation>
      
      <presentation id="DisableCustomRoot_Pres">
        <text>Geef de tenant-GUID en de waarde van de instelling op. 1 om deze instelling in te schakelen, 0 om deze instelling uit te schakelen </text>
        <text>Typ in het naamveld de tenant-GUID. Voer in het waardeveld 1 of 0 in.</text>
        <listBox refId="DisableCustomRootList">Tenantpaden: </listBox>
      </presentation>
      
      <presentation id="AllowTenantList_Pres">
        <text>Tenant-id opgeven</text>
        <text>Typ in het waardeveld de tenant-id die u wilt toevoegen aan deze lijst</text>
        <text> </text>
        <listBox refId="AllowTenantListBox">GUID van de tenant: </listBox>
      </presentation>
      
      <presentation id="BlockTenantList_Pres">
        <text>Tenant-id opgeven</text>
        <text>Typ in het waardeveld de tenant-id die u wilt toevoegen aan deze lijst</text>
        <text> </text>
        <listBox refId="BlockTenantListBox">GUID van de tenant: </listBox>
       </presentation>

      <presentation id="SharePointOnPremFrontDoorUrl_Pres">
        <text>Geef een URL naar de on-premises SharePoint-server waarop het OneDrive voor Bedrijven-account van de gebruiker wordt gehost op, en ook de naam van de tenantmap.</text>
        <textBox refId="SharePointOnPremFrontDoorUrlBox">
          <label>URL voor on-premises SharePoint-server:</label>
        </textBox>
        <textBox refId="SharePointOnPremTenantNameBox">
          <label>Naam van tenantmap:</label>
        </textBox>
      </presentation>

      <presentation id="SharePointOnPremPrioritization_Pres">
        <dropdownList refId="SharePointOnPremPrioritization_Dropdown" noSort="true" defaultItem="0">SharePoint-prioriteitstelling voor klanten van de hybride versie</dropdownList>
      </presentation>
      
      <presentation id="BlockKnownFolderMove_Pres">
        <dropdownList refId="BlockKnownFolderMove_Dropdown" noSort="true" defaultItem="0">Als bekende mappen al zijn verplaatst naar OneDrive:</dropdownList>
      </presentation>
      
      <presentation id="KFMOptInWithWizard_Pres">
        <textBox refId="KFMOptInWithWizard_TextBox">
          <label>Tenant-id:</label>
        </textBox>
      </presentation> 
      
      <presentation id="KFMOptInNoWizard_Pres">
        <textBox refId="KFMOptInNoWizard_TextBox">
          <label>Tenant-id:</label>
        </textBox>
        <dropdownList refId="KFMOptInNoWizard_Dropdown" noSort="true" defaultItem="0">Melding weergeven voor gebruikers nadat mappen zijn omgeleid:</dropdownList>
      </presentation>
      <presentation id="AutoMountTeamSites_Pres">
        <text>Een bibliotheek opgeven om te synchroniseren:

Open een webbrowser, meld u als een globale beheerder of SharePoint-beheerder voor uw organisatie aan bij Office 365, en ga naar de bibliotheek. 

Klik op de knop Synchroniseren in de bibliotheek die u automatisch wilt synchroniseren en klik vervolgens op Bibliotheek-id kopiëren. 

Klik op Weergeven om de bibliotheek-id samen met en een id in te voeren in het naamveld.
        </text>
        <listBox refId="AutoMountTeamSitesListBox">Bibliotheken</listBox>
      </presentation>
    </presentationTable>
  </resources>
</policyDefinitionResources>
