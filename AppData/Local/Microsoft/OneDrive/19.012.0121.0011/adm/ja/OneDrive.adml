<?xml version="1.0" encoding="utf-8"?>
<!-- (c) 2016 Microsoft Corporation -->
<policyDefinitionResources xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" revision="1.0" schemaVersion="1.0" xmlns="http://www.microsoft.com/GroupPolicy/PolicyDefinitions">
  <displayName>OneDrive グループ ポリシーの設定</displayName>
  <description>OneDrive 同期クライアント (OneDrive.exe) のさまざまなグループ ポリシー設定です。特にクライアントのエンタープライズ機能向けの設定です。</description>
  <resources>
    <stringTable>
      <!-- general -->
      <string id="OneDriveNGSCSettingCategory">OneDrive</string>

      <!-- block syncing personal OneDrive -->
      <string id="DisablePersonalSync">ユーザーが個人用の OneDrive アカウントを同期できないようにする</string>
      <string id="DisablePersonalSync_help">この設定は、ユーザーが Microsoft アカウントにサインインして個人用の OneDrive ファイルを同期できないようブロックすることができます。

この設定を有効にすると、ユーザーは個人用 OneDrive アカウントで同期リレーションシップを設定できなくなります。この設定を有効にしたときに既に個人用 OneDrive アカウントを同期しているユーザーは、引き続き同期することはできなくなりますが (また同期が停止したメッセージが表示されます)、コンピューターと同期されたファイルはコンピューター上に残ります。

この設定を無効にするか構成しない場合、ユーザーは個人用 OneDrive アカウントを同期できます。</string>

      <!-- turn on enterprise tier cadence for app updates -->
      <string id="EnableEnterpriseUpdate">エンタープライズ リング上の OneDrive 同期クライアントの更新プログラムを受け取る</string>
      <string id="EnableEnterpriseUpdate_help">この設定では、組織内のユーザーに対して更新リングを指定できます。OneDrive 同期クライアント (OneDrive.exe) への更新プログラムは、2 つのリング (プロダクション リング、次にエンタープライズ リング) を通じて一般公開されます。

この設定を有効にすると、OneDrive 同期クライアントは、更新プログラムがエンタープライズ リングに対して有効になるまで、更新プログラムを受信しません。これにより、更新プログラムについて十分な準備時間を設けることができますが、ユーザーは最新の修正内容を受け取るまで待つ必要があります。エンタープライズ リングでは、社内ネットワークの場所からお好きなタイミングで更新プログラムを展開することもできます。

この設定を無効にするか構成しない場合、OneDrive 同期クライアントは更新プログラムがプロダクション リングで有効になり次第、すぐに更新されます。</string>

      <!-- turn on GPOSetUpdateRing for app updates -->
      <string id="GPOSetUpdateRing">同期クライアントの更新リングを設定する</string>
      <string id="GPOSetUpdateRing_help">OneDrive 同期クライアント (OneDrive.exe) への更新プログラムが 3 つの更新リング (初めに Insider、次に Production、最後に Enterprise) の 3 つの更新リングより公開されます。この設定により、組織内のユーザーの同期クライアントのバージョンを指定できます。この設定を有効にしてリングを選択すると、ユーザーは変更できません。

Insider リングのユーザーは、OneDrive に予定されている新機能をプレビューできるビルドを受信します。

Production リングのユーザーは、利用できるようになり次第、最新機能を受け取ります。

Enterprise リングのユーザーは、新しい機能、バグ修正、パフォーマンスの改善を受け取ります。このリングでは、社内のネットワークから更新プログラムを展開し、展開のタイミングを制御できます (60 日以内)。

この設定を無効にするか構成しない場合、ユーザーは Production リングで利用できるようになり次第、OneDrive 同期クライアントの更新プログラムを受信します。ユーザーは Office または Windows Insider プログラムに登録して、Insider リングで更新プログラムを受け取ることができます。</string>

      <string id="Enterprise">エンタープライズ</string>
      <string id="Production">実稼働環境</string>
      <string id="Insider">Insider</string>

      <!-- set default location of the OneDrive folder -->
      <string id="DefaultRootDir">OneDrive フォルダーの既定の場所を設定</string>
      <string id="DefaultRootDir_help">この設定では、ユーザーのコンピューター上の OneDrive フォルダーの既定の場所として、特定のパスを設定できます。既定では、パスは %userprofile% の下になります。

この設定を有効にすると、OneDrive - {organization name} フォルダーの既定の場所は、OneDrive.admx ファイルで指定したパスになります。指定した場所をユーザーが変更できないようにするには、“ユーザーが OneDrive フォルダーの場所を変更できないようにする” 設定を有効にしてください。

この設定を無効にするか構成しない場合、OneDrive - {organization name} フォルダーの既定の場所は %userprofile% 内になります。</string>

      <!-- disable changing the default location of the OneDrive folder -->
      <string id="DisableCustomRoot">ユーザーが OneDrive フォルダーの場所を変更できないようにする</string>
      <string id="DisableCustomRoot_help">この設定では、ユーザーが OneDrive 同期クライアントの設定中に OneDrive - {organization name} フォルダーの場所を変更できないようにブロックすることができます。

この設定を有効にすると、[場所の変更] リンクが OneDrive セットアップで非表示になります。OneDrive フォルダーは既定の場所に作成されるか、[OneDrive フォルダーの既定の場所を設定する] 設定を有効にしている場合は、指定したユーザー設定の場所に作成されます。

この設定を無効にするか構成しない場合、ユーザーは [場所の変更] リンクをクリックして OneDrive セットアップで OneDrive フォルダーの場所を変更できます。</string>

      <!-- Enable Office Integration for coauthoring and in-app sharing -->
      <string id="EnableAllOcsiClients">Office デスクトップ アプリで共同編集して共有</string>
      <string id="EnableAllOcsiClients_help">この設定では、複数のユーザーが Office 365 ProPlus、Office 2019、または Office 2016 デスクトップ アプリを使用して、OneDrive に保存された Office ファイルを同時に編集することができるようになります。ユーザーは、Office デスクトップ アプリからのファイルを共有することもできます。

この設定を有効にすると、Office デスクトップ アプリの共同編集と共有の機能が有効になります。ユーザーは、OneDrive 同期クライアントの設定を開き、[Office] タブをクリックし、[Office アプリケーションを使用して自分が開いた Office ファイルを同期する] チェックボックスをオフにすることで、これらの機能を無効にすることができます。

この設定を無効にすると、Office デスクトップ アプリの共同編集と共有は無効になり、同期クライアント設定の [Office] タブは非表示になります。[Office ファイルの競合] 設定も無効になり、2 つのファイル間で競合が発生すると、両方のコピーが保持されます。</string>


      <!-- Enable hold the file for handling Office conflicts -->
      <string id="EnableHoldTheFile">Office ファイルの同期の競合を処理する方法をユーザーが選択できるようにする</string>
      <string id="EnableHoldTheFile_help">この設定では、同期中に Office ファイル間で競合が発生した場合の処理方法について指定します。既定では、変更内容を結合するか両方のコピーを保持するかについて、ユーザーが決定できます。ユーザーは、両方のコピーを常に保持するよう、OneDrive 同期クライアントの設定を変更することもできます。(このオプションは Office 2016 以降でのみ使用できます。以前のバージョンの Office では、両方のコピーが常に保持されます。)

この設定を有効にするか構成しない場合、ユーザーは変更内容を結合するか両方のコピーを保持するかを決定できます。ユーザーは OneDrive 同期クライアントの設定で両方のコピーを保持するよう選択することもできます。

この設定を無効にすると、ファイル間で競合が発生した場合にファイルの両方のコピーが保持されます。ユーザーは設定を変更したり変更内容を結合したりすることはできません。</string>

      <!-- Enable Automatic Upload Bandwidth Limiting -->
      <string id="AutomaticUploadBandwidthPercentage">同期クライアントのアップロード速度をスループットのパーセンテージまでに制限する</string>
      <string id="AutomaticUploadBandwidthPercentage_help">この設定では、コンピューターのアップロード スループットのパーセンテージを指定することで、OneDrive 同期クライアント (OneDrive.exe) がファイルのアップロードに使用できるように、コンピューター上のさまざまなアップロード タスクのパフォーマンスのバランスを調整することができます。これをパーセンテージで設定すると、同期クライアントはスループットの増減両方に対応できます。パーセンテージを低く設定すると、ファイルのアップロードが低速になります。50% 以上の値にすることをお勧めします。同期クライアントは 1 分間の制限なしに定期的にアップロードします。その後、設定したアップロード パーセンテージにまでスローダウンします。これにより、サイズの小さいファイルはすばやくアップロードされ、サイズの大きいファイルがコンピューターのアップロード スループットを独占しなくなります。

        この設定を有効にすると、コンピューターは OneDrive にファイルをアップロードするときに、指定したアップロード スループットのパーセンテージを使用します。ユーザーはこれを変更することはできません。

        この設定を無効にするか構成しない場合、ユーザーはアップロード速度を固定値 (KB/秒) に制限するかどうかを選択できます。または、[自動で調整] に設定してアップロード スループットの 70% を使用し、スループットの増減両方に対応できるようにすることもできます。

        重要: この設定を有効または無効にして、[未構成] に戻すと、最後の構成が引き続き機能します。[同期クライアントのアップロード速度を固定速度に制限する] を有効にしてアップロード速度を制限するのではなく、この設定を有効にすることをおすすめします。また、両方の設定を同時に有効にすることはできません。</string>
      <!-- Enable Upload Bandwidth Limiting -->
      <string id="UploadBandwidthLimit">同期クライアントのアップロード速度を固定速度に制限する</string>
      <string id="UploadBandwidthLimit_help">この設定では、OneDrive 同期クライアント (OneDrive.exe) がファイルをアップロードする最高速度を設定できます。この速度は固定値 (KB/秒) です。速度を低くすると、コンピューターがファイルをアップロードする速度は低下します。設定できる最低速度は 1 KB/秒で、最高速度は 100000 KB/秒です。50 KB/秒を下回る速度を入力して、UI に入力した速度が表示されていても、50 KB/秒に制限されます。

この設定を有効にすると、コンピューターは指定した最高アップロード速度を使用します。ユーザーは OneDrive の設定でこれを変更することはできません。

この設定を無効にするか構成しない場合、ユーザーはアップロード速度を固定値 ( KB/秒) に制限するか、または [自動で調整] に設定して 70% のアップロード スループットを使用してスループットの増減両方に対応できます。

この設定をアップロード速度の制限に使用するのではなく、[同期クライアントのアップロード速度をスループットのパーセンテージに制限する] を有効にして、条件の変化に応じて調整できるよう制限を設定することをお勧めします。両方の設定を同時に有効にすることはできません。</string>

      <!-- Enable Download Bandwidth Limiting -->
      <string id="DownloadBandwidthLimit">同期クライアントのダウンロード速度を固定速度に制限する</string>
      <string id="DownloadBandwidthLimit_help">この設定では、OneDrive 同期クライアント (OneDrive.exe) がファイルをダウンロードする最高速度を設定できます。この速度は固定値 (KB/秒) で、同期にのみ適用されます (更新プログラムのダウンロードには適用されません)。速度を低くすると、ファイルをダウンロードする速度は低下します。設定できる最低速度は 1 KB/秒で、最高速度は 100000 KB/秒です。50 KB/秒を下回る速度を入力して、UI に入力した速度が表示されていても、50 KB/秒に制限されます。
この設定を有効にすると、コンピューターは指定した最高ダウンロード速度を使用します。ユーザーはこれを変更することはできません。

この設定を無効にするか構成しない場合、ユーザーはダウンロード速度を OneDrive 同期クライアントの設定で制限するかどうかを選択できます。</string>

      <!-- turn off remote access/fetch on the computer  -->
      <string id="RemoteAccessGPOEnabled">ユーザーがリモートからファイルを取得できないようにする</string>
      <string id="RemoteAccessGPOEnabled_help">この設定を使用すると、個人用の OneDrive アカウントで OneDrive 同期クライアント (OneDrive.exe) にサインインしているときに、ユーザーが取得機能を使用できないようにすることができます。取得機能は、ユーザーが OneDrive.com にアクセスして、現在オンラインで OneDrive 同期クライアントを実行している Windows コンピューターを選択し、そのコンピューターのファイルすべてにアクセスできます。既定では、ユーザーは取得機能を使用できます。

この設定を有効にすると、ユーザーは取得機能を使用できなくなります。

この設定および [ユーザーが個人用 OneDrive アカウントを同期できないようにする] を無効にするか構成しない場合、ユーザーは取得機能を使用できます。

この設定は、32 ビットまたは 64 ビット バージョンの Windows を実行しているコンピューターで使用できます。</string>

      <!-- prevent OneDrive sync client (OneDrive.exe) from generating network traffic (checking for updates, etc.) until the user signs in to OneDrive -->
      <string id="PreventNetworkTrafficPreUserSignIn">ユーザーがサインインするまで同期クライアントがネットワーク トラフィックを生成できないようにする</string>
      <string id="PreventNetworkTrafficPreUserSignIn_help">この設定では、ユーザーが同期クライアントにサインインするか、コンピューターとファイルの同期を開始するまで、OneDrive 同期クライアント (OneDrive.exe) でネットワーク トラフィックが生成されないようにすることができます (更新プログラムのチェックなど)。

この設定を有効にすると、同期クライアントを自動的に開始するには、ユーザーはコンピューターの OneDrive 同期クライアントにサインインするか、コンピューターの OneDrive または SharePoint ファイルとの同期を選択する必要があります。

この設定を有効または無効にし、その後 [未構成] に戻した場合、最後の構成がそのまま使用されます。</string>

      <!-- Silent Account Config -->
      <string id="SilentAccountConfig">Windows 資格情報を使用して OneDrive 同期クライアントにユーザーをサイレント モードでサインインする</string>
      <string id="SilentAccountConfig_help">この設定では、Windows 資格情報を使用して OneDrive 同期クライアント (OneDrive.exe) にユーザーをサイレント モードでサインインすることができます。

この設定を有効にすると、プライマリ Windows アカウント (PC をドメインに参加させるときに使用したアカウント) で PC にサインインしたユーザーが、アカウントの資格情報を入力せずに同期クライアントをセットアップできます。ユーザーは OneDrive セットアップに表示されるため、同期するフォルダーを選択して OneDrive フォルダーの場所を変更することができます。ユーザーが以前の OneDrive for Business の同期クライアント (Groove.exe) を使用している場合、新しい同期クライアントがユーザーの OneDrive の同期を以前のクライアントから引き継ぎ、ユーザーの同期設定を維持します。この設定は、ファイル オンデマンドを使用していない PC での [ユーザーが自動的にダウンロードできる OneDrive の最大サイズを設定する]、および  [OneDrive フォルダーの既定の場所を設定する] と一緒によく使用されます。

この設定を無効にするか構成しない場合、ユーザーは同期を設定するのに職場または学校アカウントを使用してサインインする必要があります。

</string>

      <!-- DiskSpaceCheckThresholdMB -->
      <string id="DiskSpaceCheckThresholdMB">ユーザーの OneDrive が自動的にダウンロードできる最大サイズを設定する</string>
      <string id="DiskSpaceCheckThresholdMB_help">この設定は、[Windows 資格情報を使用して OneDrive 同期クライアントにユーザーをサイレント モードでサインインする] と組み合わせて使用されます。この設定により、OneDrive で指定した数量を超えるコンテンツを所有するユーザーに、OneDrive 同期クライアント (OneDrive.exe) のセットアップ中に同期するフォルダーを選択するメッセージを表示することができます。

この設定を有効にすると、OneDrive のセットアップ中に指定した値以上のコンテンツを所有するユーザーには、既定で [フォルダーの選択] ダイアログ ボックスが表示されます。

この設定を無効にするか構成しない場合、ユーザーが同期クライアントをセットアップ中にすべてのファイルが同期用に選択されます。</string>

      <!-- Settings below control behavior of Files-On-Demand (Cloud Files) -->
      <string id="FilesOnDemandEnabled">OneDrive ファイル オンデマンドを使用する</string>
      <string id="FilesOnDemandEnabled_help">この設定を使用すると、OneDrive ファイル オンデマンドを組織で有効にするかどうかを制御できます。

この設定を有効にすると、OneDrive ファイル オンデマンドは既定でオンになります。

この設定を無効にした場合、ユーザーはオンにすることはできません。

この設定を構成しない場合、ユーザーは OneDrive ファイル オンデマンドをオンまたはオフにすることができます。</string>

      <string id="DehydrateSyncedTeamSites">同期済みチーム サイトのファイルをオンライン専用ファイルに変換する</string>
      <string id="DehydrateSyncedTeamSites_help">この設定は、OneDrive ファイル オンデマンドと組み合わせて使用されます。多くのユーザーが同じチーム サイトを同期している場合、この設定により、同期済みチーム サイトのファイルをオンライン専用ファイルとしてマークすることで、帯域幅を節約し、デバイスの領域を解放することができます。

この設定を有効にすると、OneDrive ファイル オンデマンドを有効にする前に同期していたチーム サイトのファイルは、オンライン専用ファイルに変換されます (1 回限りのアクション)。

この設定を無効にするか構成しない場合、チーム サイトのファイルは、ユーザーがオンライン専用のファイルにしない限り、ローカルに残ります。</string>

    <!-- Restrict syncing with other organizations -->
      <string id="AllowTenantList">特定の組織にのみ OneDrive アカウントの同期を許可する</string>
      <string id="AllowTenantList_help">この設定を使用すると、許可済みテナント ID を指定することで、ユーザーが簡単にほかの組織にファイルをアップロードできないようにすることができます。

この設定を有効にすると、ユーザーが許可されていない組織からアカウントを追加しようとすると、エラーが表示されます。ユーザーがアカウントを追加済みの場合は、ファイルの同期は停止します。

この設定を無効にするか構成しない場合、ユーザーはどの組織のアカウントでも追加できます。

特定の組織をブロックするには、[特定の組織の OneDrive アカウントの同期をブロックする] を使用します。

この設定は、[特定の組織の OneDrive アカウントの同期をブロックする] よりも優先されます。両方のポリシーを同時に有効にしないでください。</string>

      <string id="BlockTenantList">特定の組織の OneDrive アカウントの同期をブロックする</string>
      <string id="BlockTenantList_help">この設定を使用すると、ブロック済みテナント ID を指定することで、ユーザーが簡単にほかの組織にファイルをアップロードできないようにすることができます。

この設定を有効にすると、ユーザーがブロックされている組織からアカウントを追加しようとすると、エラーが表示されます。ユーザーがアカウントを追加済みの場合は、ファイルの同期は停止します。

この設定を無効にするか構成しない場合、ユーザーはどの組織のアカウントでも追加できます。

許可済み組織のリストを指定するには、[特定の組織の OneDrive アカウントの同期のみを許可する] を使用します。

この設定は、[特定の組織の OneDrive アカウントの同期のみを許可する] が有効になっている場合には、機能しません。両方のポリシーを同時に有効にしないでください。</string>

    <!-- SharePoint Server 2019 settings -->
    <string id="SharePointOnPremFrontDoorUrl">SharePoint Server URL と組織名を指定する</string>
    <string id="SharePointOnPremFrontDoorUrl_help">この設定では、ユーザーが OneDrive 同期クライアント (OneDrive.exe) を使用して SharePoint Server 2019 のファイルを同期できるようにすることができます。URL は SharePoint Server の場所を定義し、同期クライアントが同期を認証してセットアップすることができます。組織名には OneDrive と SharePoint フォルダー名を指定することができ、それがファイル エクスプローラー内に作成されます。組織名は省略可能です。組織名を指定しない場合、同期クライアントは URL の最初のセグメントを名前として使用します。たとえば、office.sharepoint.com は “Office” になり、OneDrive フォルダー名は "OneDrive - Office" になります。

この設定を有効にして、SharePoint Server URL を指定した場合、ユーザーは SharePoint Server 2019 のファイルを同期できるようになります。

この設定を無効にするか構成しない場合、または SharePoint Server URL を指定しない場合、ユーザーは SharePoint Server 2019 のファイルを同期することはできません。</string>

    <string id="SharePointOnPremPrioritization">ハイブリッド環境で OneDrive の場所を指定する</string>
    <string id="SharePointOnPremPrioritization_help">この設定では、OneDrive 同期クライアント (OneDrive.exe) が SharePoint Online または SharePoint Server 2019 に対して、その両方の ID プロバイダーに ID が存在する場合に、認証するかどうかを定義します。この設定を使用するには、[SharePoint Server URL と組織名を指定する] も有効にする必要があります。この設定は、OneDrive for Business 同期機能にのみ影響します。SharePoint Online または SharePoint Server 2019 のチーム サイトの同期には影響を与えません。

この設定を有効にすると、次の 2 つのオプションから選択できます:

SharePoint Online: 同期クライアントは最初に SharePoint Online でユーザーの OneDrive を探します。同期クライアントが既にそのユーザーの SharePoint Online で構成されている場合は、そのユーザーの SharePoint Server 2019 の OneDrive for Business インスタンスの構成を試みます。

SharePoint Server 2019: 同期クライアントは最初に SharePoint Server 2019 でユーザーの OneDrive for Business を探します。同期クライアントがそのユーザーの SharePoint Server 2019 で既に構成済みの場合、そのユーザーの SharePoint Online で OneDrive インスタンスの構成を試みます。

この設定を無効にするか構成しない場合、同期クライアントは最初に SharePoint Online でユーザーの OneDrive を探します。</string>
    <string id="PrioritizeSPO">SharePoint Online</string>
    <string id="PrioritizeSharePointOnPrem">SharePoint Server 2019</string>

    <!-- Disable tutorial in the FRE -->
    <string id="DisableFRETutorial">OneDrive セットアップの最後に表示されるチュートリアルを無効にする</string>
    <string id="DisableFRETutorial_help">この設定では、OneDrive のセットアップの最後に Web ブラウザーにチュートリアルが表示されないようにすることができます。

この設定を有効にすると、ユーザーには OneDrive のセットアップが完了した後にチュートリアルが表示されなくなります。

この設定を無効にするか構成しない場合、OneDrive セットアップの最後にチュートリアルが表示されます。</string>

      <!-- Block KFM -->
      <string id="BlockKnownFolderMove">ユーザーが Windows の既知のフォルダーから OneDrive に移動できないようにする</string>
      <string id="BlockKnownFolderMove_help">これは、ユーザーが自分の [ドキュメント]、[ピクチャ]、[デスクトップ] のフォルダーをどの OneDrive for Business アカウントにも移動できないようにするための設定です。
注: 既知のフォルダーを個人用 OneDrive アカウントに移動することは、ドメインに参加している PC ではすでにブロックされています。

この設定を有効にすると、ユーザーには [重要なフォルダーの保護を設定] ウィンドウが表示されず、[保護の開始] コマンドが無効になります。ユーザーがすでに既知のフォルダーを移動している場合、そのフォルダー内のファイルは OneDrive に残ります。この設定は、[Windows の既知のフォルダーを OneDrive に移動するメッセージをユーザーに表示する] または [サイレント モードで Windows の既知のフォルダーを OneDrive に移動する] を有効にしている場合には機能しません。

この設定を無効にした場合や構成しない場合は、ユーザーが自分の既知のフォルダーを移動できます。</string>

    <!-- KFMOptInWithWizard -->
    <string id="KFMOptInWithWizard">Windows の既知のフォルダーを OneDrive に移動するメッセージをユーザーに表示する</string>
    <string id="KFMOptInWithWizard_help">これは、[IT 部門はあなたに重要なフォルダーの保護を要求しています] ウィンドウが表示されるようにするための設定です。このウィンドウでは、ユーザーが自分の [ドキュメント]、[ピクチャ]、[デスクトップ] のフォルダーを OneDrive に移動できます。この設定はオンプレミスの SharePoint では有効になりません。

この設定を有効にしてテナント ID を入力すると、現在自分の OneDrive を同期しているユーザーがサインインしたときに [IT 部門はあなたに重要なフォルダーの保護を要求しています] ウィンドウが表示されます。このウィンドウを閉じるとアクティビティ センターにアラーム通知が表示され、これはそのユーザーが 3 つの既知のフォルダーすべてを移動するまで続きます。ユーザーが既に既知のフォルダーを別の OneDrive アカウントにリダイレクトしている場合は、フォルダーのリダイレクト先を組織のアカウントにするよう指示する画面が表示されます (既存のファイルはそのまま残ります)。

この設定を無効にした場合や構成しない場合は、[IT 部門はあなたに重要なフォルダーの保護を要求しています] ウィンドウが自動的に表示されることはありません。</string>

    <!-- KFMOptInNoWizard -->
    <string id="KFMOptInNoWizard">サイレント モードで Windows の既知のフォルダーを OneDrive に移動する</string>
    <string id="KFMOptInNoWizard_help">この設定では、ユーザーが操作することなく、既知のフォルダーから OneDrive にリダイレクトすることができます。同期クライアントのビルド 18.171.0823.0001 より前の場合、この設定により空の既知のフォルダーから OneDrive (または別の別の OneDrive アカウントに既にリダイレクトしていた既知のフォルダー) へのリダイレクトのみが行われます。それ以降のビルドでは、コンテンツを含む既知のフォルダーにリダイレクトし、コンテンツが OneDrive に移動されます。この設定は、[Windows の既知のフォルダーを OneDrive に移動するメッセージをユーザーに表示する] と一緒に使用することをおすすめします。既知のフォルダーをサイレント モードで移動することに失敗すると、ユーザーにはエラーを修正して続行するよう示すメッセージが表示されます。

この設定を有効にしてテナント ID を指定すると、フォルダーがリダイレクトされた後に通知を表示するかどうかを選択できます。

この設定を無効にするか構成しない場合、ユーザーの既知のフォルダーはサイレント モードで OneDrive にリダイレクトまたは移動されません。

</string>
    <string id="KFMOptInNoWizardToast">はい</string>
    <string id="KFMOptInNoWizardNoToast">いいえ</string>

     <!-- Block KFM Opt Out -->
      <string id="KFMBlockOptOut">ユーザーが Windows の既知のフォルダーから PC にリダイレクトできないようにする</string>
      <string id="KFMBlockOptOut_help">これは、ユーザーの [ドキュメント]、[ピクチャ]、[デスクトップ] のフォルダーを OneDrive にリダイレクトすることを強制的に継続するための設定です。

この設定を有効にすると、[IT 部門はあなたに重要なフォルダーの保護を要求しています] ウィンドウの [保護の停止] ボタンが無効になり、ユーザーが既知のフォルダーの同期を停止しようとすると、エラーが表示されます。

この設定を無効にした場合や構成しない場合は、ユーザーが既知のフォルダーを自分の PC に戻すことができます。 </string>

    <string id="AutoMountTeamSites">チーム サイト ライブラリを自動的に同期するように構成する</string>
    <string id="AutoMountTeamSites_help">この設定を使用すると、次回ユーザーが OneDrive 同期クライアント (OneDrive.exe) にサインインしたときに SharePoint チーム サイト ライブラリを自動的に同期するように指定することができます。ユーザーがサインインした後、ライブラリが同期を開始するまで、最大 8 時間かかることがあります。設定を使用するには、OneDrive ファイル オンデマンドを有効にする必要があります。設定は、Windows 10 (1709) Fall Creators Update 以降を実行しているコンピューターのユーザーにのみ適用されます。この設定は、1000 を超えるデバイスに対する同じライブラリについて有効にしないでください。この機能は、オンプレミスの SharePoint サイトでは有効になっていません。
 
この設定を有効にすると、OneDrive 同期クライアントは、次回ユーザーがサインインしたときにオンライン専用ファイルとして指定されたライブラリのコンテンツを自動的にダウンロードします。ユーザーがライブラリの同期を停止することはできません。
 
この設定を無効にすると、指定したチーム サイト ライブラリは新しいユーザーでは自動的には同期されません。既存のユーザーは、ライブラリの同期を停止することを選択できますが、ライブラリの同期は自動的には停止しません。</string>

    <!-- Disable Pause On Battery Saver  -->
    <string id="DisablePauseOnBatterySaver">デバイスのバッテリー節約機能モードがオンのときに同期を続ける</string>
    <string id="DisablePauseOnBatterySaver_help">この設定は、バッテリー節約機能モードがオンになっているデバイスの自動一時停止機能をオフにすることができます。
この設定を有効にすると、ユーザーがバッテリー節約機能モードをオンにしたときに同期が継続されます。OneDrive は自動的に同期を一時停止することはありません。
この設定を無効にするか設定しない場合、バッテリー節約機能モードが検出されたときに同期が自動的に一時停止し、通知が表示されます。
ユーザーは、通知で [同期を続ける] をクリックすることで、同期を一時停止しないように選択できます。
同期が一時停止すると、ユーザーは、タスク バーの通知領域にある OneDrive クラウド アイコンをクリックして、次にアクティビティ センターの上部にあるアラートをクリックすることで、同期を再開できます。</string>

    <!-- Disable Pause On Metered Network -->
    <string id="DisablePauseOnMeteredNetwork">従量制課金ネットワークのときにも同期を続ける</string>
    <string id="DisablePauseOnMeteredNetwork_help">この設定を使用すると、デバイスが従量制課金ネットワークに接続されているときに自動一時停止機能をオフにすることができます。

この設定を有効にすると、デバイスが従量制課金ネットワークに接続されているときにも同期は継続されます。OneDrive は同期を自動的に一時停止することはありません。

この設定を無効にするか設定しない場合、従量制課金ネットワークが検出されたときに同期が自動的に一時停止し、通知が表示されます。ユーザーは、通知で [同期を続ける] をクリックすることで、同期を一時停止しないよう選択することができます。同期が一時停止されている場合、ユーザーはタスク バーの通知領域にある OneDrive クラウド アイコンをクリックして、次にアクティビティ センターの上部にあるアラートをクリックすることで、同期を再開できます。</string>

    <!-- Insert multi-tenant settings here -->
    <!-- See http://go.microsoft.com/fwlink/p/?LinkId=797547 for configuration instructions -->
<!-- Commented out before strings finalized.
    <string id="LocalMassDeleteFileDeleteThreshold">Count of file deletes to determine if there is a local mass delete.</string>
    <string id="LocalMassDeleteFileDeleteThreshold_help">If qualified file deletes are more than specified threshold, user will be notified before client sending the deletes to the cloud. User can confirm the deletes or restore the files. Specify 0 to opt out the feature.</string> -->
    </stringTable>
    <presentationTable>
      <presentation id="GPOSetUpdateRing_Pres">
        <dropdownList refId="GPOSetUpdateRing_Dropdown" noSort="true" defaultItem="0">更新リング:</dropdownList>
      </presentation>

      <presentation id="AutomaticUploadBandwidthPercentage_Pres">
        <text>ファイルをアップロードするときに使用する帯域幅の最大パーセンテージを選択します。</text>
        <text>有効な値は 10 から 99 です。</text>
        <decimalTextBox refId="BandwidthSpinBox" defaultValue="70" spinStep="1">帯域幅:</decimalTextBox>
      </presentation>

      <presentation id="UploadBandwidthLimit_Pres">
        <text>ファイルをアップロードするときに使用する帯域幅の最大量を選択します。</text>
        <text>有効な値は 1 から 100000 です。</text>
        <decimalTextBox refId="UploadRateValue" defaultValue="125">帯域幅:</decimalTextBox>
      </presentation>

      <presentation id="DownloadBandwidthLimit_Pres">
        <text>ファイルをダウンロードするときに使用する帯域幅の最大量を選択します。</text>
        <text>有効な値は 1 から 100000 です。</text>
        <decimalTextBox refId="DownloadRateValue" defaultValue="125">帯域幅:</decimalTextBox>
      </presentation>

       <presentation id="DiskSpaceCheckThresholdMB_Pres">
        <text>ユーザーに同期するフォルダーを選択するメッセージを表示する前に、テナント ID と ユーザーの OneDrive の最大サイズを指定する</text>
        <text>[値名] フィールドにテナント ID を入力します。[値] フィールドにサイズを入力します。</text>
        <text>有効な値は 0 から 4294967295 MB (4294967295 を含む) です。</text>
        <listBox refId="DiskSpaceCheckThresholdMBList">最大サイズ:</listBox>
      </presentation>

      <presentation id="DefaultRootDir_Pres">
       <text>テナントの ID と既定のパスを指定します。</text>
        <text>[値名] フィールドにテナント ID を入力します。[値] フィールドにパスを入力します。</text>
        <listBox refId="DefaultRootDirList">パス: </listBox>
      </presentation>

      <presentation id="DisableCustomRoot_Pres">
        <text>テナント ID と、設定の値を指定します。</text>
        <text>[値名] フィールドにテナント ID を入力します。[値] フィールドに、1 を入力して設定を有効にし、0 を入力して設定を無効にします。</text>
        <listBox refId="DisableCustomRootList">場所の設定を変更:</listBox>
      </presentation>

      <presentation id="AllowTenantList_Pres">
        <text>テナント ID を指定</text>
        <text>[値] フィールドに、このリストに追加するテナント ID を入力します</text>
        <text> </text>
        <listBox refId="AllowTenantListBox">テナント ID:</listBox>
      </presentation>

      <presentation id="BlockTenantList_Pres">
        <text>テナント ID を指定</text>
        <text>[値] フィールドに、このリストに追加するテナント ID を入力します。</text>
        <text> </text>
        <listBox refId="BlockTenantListBox">テナント ID:</listBox>
       </presentation>

      <presentation id="SharePointOnPremFrontDoorUrl_Pres">
        <text>ユーザーの OneDrive for Business をホストしている SharePoint Server への URL と、組織名を指定してください。</text>
        <textBox refId="SharePointOnPremFrontDoorUrlBox">
          <label>SharePoint Server 2019 の URL:</label>
        </textBox>
        <textBox refId="SharePointOnPremTenantNameBox">
          <label>組織名:</label>
        </textBox>
      </presentation>

      <presentation id="SharePointOnPremPrioritization_Pres">
        <dropdownList refId="SharePointOnPremPrioritization_Dropdown" noSort="true" defaultItem="0">初めて認証を行う対象:</dropdownList>
      </presentation>

      <presentation id="BlockKnownFolderMove_Pres">
        <dropdownList refId="BlockKnownFolderMove_Dropdown" noSort="true" defaultItem="0">既知のフォルダーが既に OneDrive に移動されている場合:</dropdownList>
      </presentation>

      <presentation id="KFMOptInWithWizard_Pres">
        <textBox refId="KFMOptInWithWizard_TextBox">
          <label>テナント ID:</label>
        </textBox>
      </presentation>

      <presentation id="KFMOptInNoWizard_Pres">
        <textBox refId="KFMOptInNoWizard_TextBox">
          <label>テナント ID:</label>
        </textBox>
        <dropdownList refId="KFMOptInNoWizard_Dropdown" noSort="true" defaultItem="0">フォルダーがリダイレクトされた後にユーザーに通知を表示:</dropdownList>
      </presentation>
      <presentation id="AutoMountTeamSites_Pres">
        <text>ライブラリを同期するように指定するには:

Web ブラウザーを開き、Office 365 にグローバルまたは組織の SharePoint 管理者でサインインし、ライブラリを参照します。

自動的に同期するライブラリで [同期] ボタンをクリックし、[ライブラリ ID のコピー] をクリックします。

[値名] フィールドにライブラリの表示名を入力し、[値] フィールドにライブラリ ID を入力します。
        </text>
        <listBox refId="AutoMountTeamSitesListBox">ライブラリ:</listBox>
      </presentation>

<!--  Commented out before strings finalized.
      <presentation id="LocalMassDeleteFileDeleteThreshold_Pres">
        <text>Set the threshold (number of file deletes) for local mass delete detection.</text>
        <text>Valid values are from 0 to 100000.</text>
        <decimalTextBox refId="LMDFileDeleteThresholdBox" defaultValue="200">Threshold:</decimalTextBox>
      </presentation>    -->
    </presentationTable>
  </resources>
</policyDefinitionResources>
